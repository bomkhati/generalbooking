<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDefaultSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_default_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('day');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->integer('space_available')->unsigned(10)->nullable();
            $table->integer('booking_interval')->nullable();
            $table->integer('total_booking')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_default_settings');
    }
}
