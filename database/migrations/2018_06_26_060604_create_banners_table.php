<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',100);
            $table->text('description')->nullable();
            $table->string('image',100);
            $table->integer('position')->unsigned(10);
            $table->string('button_text', 100)->nullable();
            $table->string('button_link', 100)->nullable();
            $table->enum('button_target', ['parent', 'newtab'])->index();
            $table->enum('status', ['active', 'in_active'])->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
