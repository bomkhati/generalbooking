<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('keyword')->nullable();
            $table->text('keyword_description')->nullable();
            $table->text('facebook_script')->nullable();
            $table->text('facebook_noscript')->nullable();
            $table->text('google_analytics_script')->nullable();
            $table->text('google_analytics_noscript')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_codes');
    }
}
