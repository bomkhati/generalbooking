<?php
class UsersTableSeeder extends \Illuminate\Database\Seeder
{

    public function run(){
        DB::table('users')->delete();

        DB::table('users')->insert(
            array(
                array(
                    'password' => bcrypt('password'),
                    'email'  => 'reception@lanailsstudio.com.au',
                    'phone'  => '(03) 9939 4498',
                    'user_type' => 'superadmin'
                )
            ));
    }

}