<?php

use Illuminate\Database\Seeder;

class SiteMaintenanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_maintenance')->delete();

        DB::table('site_maintenance')->insert(
            array(
                array(
                    'title' => 'Site Maintenance Mode',
                    'status' => 'in_active',

                )
            ));
    }
}
