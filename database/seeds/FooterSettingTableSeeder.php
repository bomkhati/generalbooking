<?php
use Illuminate\Database\Seeder;

class FooterSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('footer_settings')->delete();

        DB::table('footer_settings')->insert(
            array(
                array(
                    'office' => 'Wildiaries',
                    'location' => 'L5/175 Flinders Lane, Melbourne 3000',
                    'phone' => '+61 (0) 405 220830',
                    'website' => 'http://wildiaries.com/',
                    'email' => 'simon@wildiaries.com',
                    'description' => 'NULL',
                    'status' => 'active'
                )
            ));
    }
}