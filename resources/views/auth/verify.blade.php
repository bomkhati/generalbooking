@extends('layouts.customer')
@section('title', 'Email Verification')
@section('content')
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr tb overlay-white-light " style="background-image:url({{asset('storage/frontweb/assets/images/banner.jpg')}});">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h2 class="text-red">Register Customer Account</h2>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <ul class="list-inline">
                            <li><a href="/">Home</a></li>
                            <li>Sign In / Sign Up Customer Account</li>
                        </ul>
                    </div>
                    <!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <div class="container section-padding">
            <div class="row justify-content-center">
                <div class="col-md-6" style="border-right: 1px solid #bbc1bf;top:3rem;">
                    <div class="card">
                        <div class="card-body" style="padding-top: 4rem;">
                            <div class="alert alert-success">
                                <p>A fresh verification link has been sent to your email address.</p>
                                <p>Before proceeding, please check your email for a verification link.</p>
                                <p>If you did not receive the email <a href="{{ route('verification.resend') }}" class="site-button">Click here to request another</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
