@extends('layouts.customer')

@section('content')

    <div class="page-content">
        <!-- Title separator style 13 -->
        <div class="section-full content-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sort-title clearfix text-center">
                            <h4><i class="fa fa-leaf" style="color:red"></i> Reset Password <i class="fa fa-leaf" style="color:red"></i></h4>
                        </div>
                    </div>
                    <div class="section-content">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <button name="submit" type="submit" value="Submit" class="site-button "> <span>Send Password Reset Link</span> </button>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Title separator style 13 END -->
    </div>
@endsection
