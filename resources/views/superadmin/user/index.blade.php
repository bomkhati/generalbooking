@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Manage CMS Users</h2>
    </div>

    <div class="panel">
        <div class="panel-body">

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $k => $user)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('user.edit', [$user->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('user.destroy',[$user->id]) }}" title="">
                                            {!! delete_form(route('user.destroy', [$user->id]))!!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $users->render() !!}
            </div>
        </div>
    </div>
@stop

