@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Add Customer / User </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" method="POST" action="{{ route('user.store') }}"
                      enctype="multipart/form-data">
                    @include('superadmin.user.form')
                </form>
            </div>
        </div>
    </div>

@stop