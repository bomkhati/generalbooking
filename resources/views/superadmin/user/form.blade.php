{!! csrf_field() !!}
<div class="form-group">
    <label class="col-sm-3 control-label">Full Name</label>

    <div class="col-sm-6">
        <input type="text" name="full_name" placeholder="Full Name" class="form-control"
               value="{{ old('full_name', isset($cmsuser->full_name) ? $cmsuser->full_name : '') }}">
        <span class="validation-error">{{ $errors->first('full_name') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Email Address</label>

    <div class="col-sm-6">
        <input type="text" name="email" class="form-control" placeholder="Email Address"
               value="{{ old('email', isset($cmsuser->email) ? $cmsuser->email : '') }}">
        <span class="validation-error">{{ $errors->first('email') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">User Name</label>

    <div class="col-sm-6">
        <input type="text" name="username" class="form-control" placeholder="User Name"
               value="{{ old('username', isset($cmsuser->username) ? $cmsuser->username : '') }}">
        <span class="validation-error">{{ $errors->first('username') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Password</label>

    <div class="col-sm-6">
        <input type="password" name="password" placeholder="Password" class="form-control" value="">
        <span class="validation-error">{{ $errors->first('password') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Confirm Password</label>

    <div class="col-sm-6">
        <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control"
               value="">
        <span class="validation-error">{{ $errors->first('password_confirmation') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"> Address</label>

    <div class="col-sm-6">
        <input type="text" name="address" placeholder="Address" class="form-control"
               value="{{ old('address', isset($cmsuser->address) ? $cmsuser->address : '') }}">
        <span class="validation-error">{{ $errors->first('address') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Phone</label>

    <div class="col-sm-6">
        <input type="text" name="phone" placeholder="Phone" class="form-control"
               value="{{ old('phone', isset($cmsuser->phone) ? $cmsuser->phone : '') }}">
        <span class="validation-error">{{ $errors->first('phone') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Select Role</label>

    <div class="col-sm-6">
        <input type="radio" name="user_type" class="checkbox-inline user_type" value="superadmin"
                {{old('user_type',isset($cmsuser->user_type)?$cmsuser->user_type:'')=='superadmin'?'checked="checked"':''}}>
        Administrator
        <input type="radio" name="user_type" class="checkbox-inline user_type" value="account"
                {{old('user_type',isset($cmsuser->user_type)?$cmsuser->user_type:'')=='account'?'checked="checked"':''}}>
        Account
        <input type="radio" name="user_type" class="checkbox-inline user_type"
               value="customer_support" @if(empty($cmsuser->user_type)) checked="checked" @endif
                {{old('user_type',isset($cmsuser->user_type)?$cmsuser->user_type:'')=='customer_support'?'checked="checked"':''}}>
        Customer Support
        <input type="radio" name="user_type" class="checkbox-inline user_type" value="supervisor"
                {{old('user_type',isset($cmsuser->user_type)?$cmsuser->user_type:'')=='supervisor'?'checked="checked"':''}}>
        Supervisor
        <input type="radio" name="user_type" class="checkbox-inline user_type" value="state"
                {{old('user_type',isset($cmsuser->user_type)?$cmsuser->user_type:'')=='state'?'checked="checked"':''}}>
        State
    </div>
</div>

<div class="custom-supervisor" style="display: none;">

    <div class="form-group">
        <label class="col-sm-3 control-label">Supervisor Level</label>

        <div class="col-sm-6">
            <select name="supervisor_level" id="supervisor-level" class="form-control">
                <option value="0"
                        {{ old('supervisor_level', !empty($cmsuser) ? $cmsuser->supervisor_level : '') == 0 ? 'selected="selected"' : ''  }}>
                    Default
                </option>
                <option value="1"
                        {{ old('supervisor_level', !empty($cmsuser) ? $cmsuser->supervisor_level : '') == 1 ? 'selected="selected"' : ''  }}>
                    Cheif
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Grade Preference</label>

        <div class="col-sm-6">
            <select name="grade_preference" id="supervisor-level" class="form-control">
                <option value="">All</option>
                @foreach($grades as $grade)
                    <option value="{{ $grade->grade_name }}" {{ old('grade_preference', !empty($cmsuser) ? $cmsuser->grade_preference : '') == $grade->grade_name ? 'selected="selected"' : ''  }}>
                        {{ $grade->grade_name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="form-group statediv">
    <label class="col-sm-3 control-label">Select State</label>

    <div class="col-sm-6">
        <select name="state_id" class="form-control" id="state">
            <option value="">Select State</option>
            @if(!empty($states))
                @foreach($states as $state)
                    <option value="{{$state->id}}" {{old('state_id',isset($cmsuser->state_id)?$cmsuser->state_id:'')==$state->id?'selected="selected"':''}}>{{ $state->name }}</option>
                @endforeach
            @endif

        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>

    <div class="col-sm-6">
        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-medium bootstrap-switch-animate">
            <div class="bootstrap-switch-container">
                <input data-on-color="primary" name="status"
                        {{ old('status', isset($cmsuser->status) ? $cmsuser->status : '') == 'active' ? 'checked' : '' }}
                @if(empty($cmsuser->status))
                       checked
                       @endif
                       class="input-switch" data-size="medium"
                       data-on-text="Active" data-off-text="Inactive"
                       type="checkbox"></div>

        </div>
        <span class="validation-error">{{ $errors->first('status') }}</span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <input type="submit" id="submit" name="" value="Save" class="btn btn-success">
        <a href="{{ route('cmsuser.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>

@section('page-specific-scripts')
    <script>

        $(document).ready(function () {

            var type = $("input[name='user_type']:checked").val();

            if (type == 'supervisor') {
                $('.custom-supervisor').show();
            } else {
                $('.custom-supervisor').hide();
            }

            if (type == 'state') {
                $('.statediv').show();
            }
            else {
                $('.statediv').hide();
            }

            $('.user_type').click(function () {

                var type = $("input[name='user_type']:checked").val();

                if (type == 'supervisor') {
                    $('.custom-supervisor').show();
                } else {
                    $('.custom-supervisor').hide();
                }

                if (type == 'state') {
                    $('.statediv').show();
                }
                else {
                    $('.statediv').hide();
                }

            });

            $('#submit').click(function () {

                var type = $("input[name='user_type']:checked").val();

                var supervisorLevel = $('#supervisor-level').val();
                console.log(supervisorLevel);
                var gradePreference = $('#grade-preference').val();
                var state = $('#state').val();

                if (type == 'supervisor' && (supervisorLevel == '' || gradePreference == '')) {

                    if (supervisorLevel == '')
                        $('#supervisor-level').css({"border": "1px solid red"});

                    if (gradePreference == '')
                        $('#grade-preference').css({"border": "1px solid red"});

                    setTimeout(function () {
                        $('#supervisor-level').css({"border": "1px solid #ddd"});
                        $('#grade-preference').css({"border": "1px solid #ddd"});
                    }, 5000);

                    return false;
                }

                if(type == 'state' && state == '')
                {
                    $('#state').css({"border": "1px solid red"});

                    setTimeout(function() {
                        $('#state').css({"border": "1px solid #ddd"});
                    }, 5000);
                    return false;
                }
            });

        });

    </script>
@endsection