{!! csrf_field() !!}
<div class="form-group">
    <label class="col-sm-3 control-label">Logo</label>

    <div class="col-sm-6">
        @if(isset($option->image))
            <img src="{{ asset($option->image_path) }}" height="40px" alt=""/>
            <br/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>


<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="company_name" class="control-label">Company Name</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="company_name" name="company_name" value="{{ old('company_name', isset($option->company_name) ? $option->company_name : '') }}" class="form-control" placeholder="Enter Your Company Name">
        <span for="company_name" class="help-block">{{ $errors->first('company_name') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Email Address</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="email_address" name="email_address" value="{{ old('email_address', isset($option->email_address) ? $option->email_address : '') }}" class="form-control" placeholder="Enter Email Address">
        <span for="email_address" class="help-block">{{ $errors->first('email_address') ? $errors->first('email_address') : 'This address is used for admin purposes, like new user notification.'}}</span>
    </div>
</div>


<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="contact_number" class="control-label">Contact Number</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="contact_number" name="contact_number" value="{{ old('contact_number', isset($option->contact_number) ? $option->contact_number : '') }}" class="form-control" placeholder="Enter Contact Number">
        <span for="contact_number" class="help-block">{{ $errors->first('contact_number') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="contact_number" class="control-label">Location</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="location" name="location" value="{{ old('location', isset($option->location) ? $option->location : '') }}" class="form-control" placeholder="Enter Location Address">
        <span for="contact_number" class="help-block">{{ $errors->first('location') }}</span>
    </div>
</div>

<div class="form-action">
    <div class="form-footer col-sm-offset-3">
        <button type="submit" class="btn btn-primary"> Save Settings</button>
    </div>
</div>