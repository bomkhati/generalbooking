@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Add General Option </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="example-box-wrapper">
                <form class="form-horizontal" id="parentPageForm" method="POST" action="{{ route('option.store') }}"
                      enctype="multipart/form-data">
                    @include('superadmin.option.form')
                </form>
            </div>
        </div>
    </div>

@stop