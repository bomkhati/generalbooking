@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Edit General Option</h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="example-box-wrapper">
                <form class="form-horizontal" method="POST" action="{{ route('option.update', $option->id) }}"
                      enctype="multipart/form-data">
                    {{ method_field('patch') }}
                    @include('superadmin.option.form')
                </form>
            </div>
        </div>
    </div>

@stop