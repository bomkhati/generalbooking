@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Manage General Option</h2>
    </div>

    <div class="panel">
        <div class="panel-body">

            @if($options->count()<1)
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('option.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add General Option Here
                </a>
            </div>
            @endif

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Company Name</th>
                    <th>Email Address</th>
                    <th>Contact</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($options as $k => $option)
                    <tr>
                        <td><img src="{{ asset($option->image_path) }}" height="40px" alt=""/></td>
                        <td>{{$option->company_name }}</td>
                        <td>{{$option->email_address }}</td>
                        <td>{{$option->contact_number }}</td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('option.edit', [$option->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $options->render() !!}
            </div>
        </div>
    </div>
@stop

