@extends('layouts.superadmin')

@section('content')
<div id="page-title">
    <h2>Add Default Booking Settings</h2>
</div>

<div class="panel">
    <div class="panel-body">
        <h3 class="title-hero">
            Basic Information
        </h3>

        <div class="example-box-wrapper">
            <form class="form-horizontal bordered-row" method="POST" action="{{ route('default-setting.store') }}"
                    enctype="multipart/form-data">
                @include('superadmin.booking.default-setting.form')
            </form>
        </div>
    </div>
</div>

@stop