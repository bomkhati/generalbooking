{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Day</label>

    <div class="col-sm-6">
        <select name="day" class="form-control" required>
            <option value="0" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '0' ? 'checked="checked"' : '' }}>Monday</option>
            <option value="1" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '1' ? 'checked="checked"' : '' }}>Tuesday</option>
            <option value="2" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '2' ? 'checked="checked"' : '' }}>Wednesday</option>
            <option value="3" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '3' ? 'checked="checked"' : '' }}>Thursday</option>
            <option value="4" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '4' ? 'checked="checked"' : '' }}>Friday</option>
            <option value="5" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '5' ? 'checked="checked"' : '' }}>Saturday</option>
            <option value="6" {{ old('status', isset($defaultSetting->day) ? $defaultSetting->day : '') == '6' ? 'checked="checked"' : '' }}>Sunday</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Opening Time</label>
    <div class="col-sm-6">
        <input type="time" name="start_time" id="start_time" value="{{ old('start_time', isset($defaultSetting->start_time) ? $defaultSetting->start_time : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('start_time') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Closing Time</label>
    <div class="col-sm-6">
        <input type="time" name="end_time" id="end_time" value="{{ old('end_time', isset($facility->end_time) ? $facility->end_time : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('end_time') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Total Booking</label>
    <div class="col-sm-6">
        <input type="text" name="total_booking" id="total_booking" value="{{ old('total_booking', isset($defaultSetting->total_booking) ? $defaultSetting->total_booking : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('total_booking') }}</i>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Booking Interval (Min)</label>
    <div class="col-sm-6">
        <input type="text" name="booking_interval" id="booking_interval" value="{{ old('booking_interval', isset($defaultSetting->booking_interval) ? $defaultSetting->booking_interval : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('booking_interval') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Space Available</label>
    <div class="col-sm-6">
        <input type="text" name="space_available" id="space_available" value="{{ old('space_available', isset($defaultSetting->space_available) ? $defaultSetting->space_available : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('space_available') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('default-setting.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
