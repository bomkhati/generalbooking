@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Default Booking Settings</h2>


    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('default-setting.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i>Add Default Booking Settings Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Day</th>
                    <th>Opening Time</th>
                    <th>Closing Time</th>
                    <th>Total Booking</th>
                    <th>Space Available</th>
                    <th>Booking Interval</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($defaultSettings->count() > 0)
                    @foreach($defaultSettings as $k => $defaultSetting)
                        <tr>
                            <td>{{ ++$k }}</td>
                            @php
                                $day = $defaultSetting->day;
                                if($day == '0'){$day = 'Monday';}
                                if($day == '1'){$day = 'Tuesday';}
                                if($day == '2'){$day = 'Wednesday';}
                                if($day == '3'){$day = 'Thursday';}
                                if($day == '4'){$day = 'Friday';}
                                if($day == '5'){$day = 'Saturday';}
                                if($day == '6'){$day = 'Sunday';}
                            @endphp
                            <td>{{ $day }}</td>
                            <td>{{ $defaultSetting->start_time }} AM</td>
                            <td>{{ $defaultSetting->end_time }} PM</td>
                            <td>{{ $defaultSetting->total_booking }}</td>
                            <td>{{ $defaultSetting->space_available }}</td>
                            <td>{{ $defaultSetting->booking_interval }}</td>

                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('default-setting.edit', [$defaultSetting->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('default-setting.destroy',[$defaultSetting->id]) }}">
                                                {!! delete_form(route('default-setting.destroy',[$defaultSetting->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $defaultSettings->render() !!}
            </div>

        </div>
    </div>
@stop
