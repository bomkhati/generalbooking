@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Booking Reports</h2>

        <p></p>
    </div>

    <div class="panel">
        <div class="panel-body">

            <div class="example-box-wrapper">
                <form class="form-horizontal" action="{{route('reports')}}" role="form">
                    <div class="form-group">
                        <div class="col-sm-2">
                        <label for="" class=" control-label">Start Date</label>

                            <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon">
                                        <i class="glyph-icon icon-calendar calendar"></i>
                                    </span>
                                <input type="text" class="bootstrap-datepicker form-control"  autocomplete="off" name="begin_date"
                                       id="begin_date"
                                       value="{{ request()->has('begin_date')?request()->get('begin_date'):'' }}"
                                       data-date-format="mm/dd/yy">
                            </div>
                        </div>

                        <div class="col-sm-2">
                        <label for="" class="control-label">End Date</label>

                            <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon">
                                        <i class="glyph-icon icon-calendar calendar"></i>
                                    </span>
                                <input type="text" class="bootstrap-datepicker form-control"  autocomplete="off" name="end_date" id="end_date"
                                       value="{{ request()->has('end_date')?request()->get('end_date'):date('Y-m-d') }}"
                                       placeholder="Pick End Date" data-date-format="mm/dd/yy">
                            </div>
                        </div>

                        <div class="col-sm-2">
                        <label for="" class="control-label">Select Month</label>

                            <div class="input-prepend input-group" >
                                <select class="form-control" name="month" id="month">
                                    <option value="">Select One</option>
                                    <option value="1" {{ request()->has('month') && request()->get('month')=='1' ?'selected="selected"':''}}>
                                        January
                                    </option>
                                    <option value="2" {{ request()->has('month') && request()->get('month')=='2' ?'selected="selected"':''}}>
                                        February
                                    </option>
                                    <option value="3" {{ request()->has('month') && request()->get('month')=='3' ?'selected="selected"':''}}>
                                        March
                                    </option>
                                    <option value="4" {{ request()->has('month') && request()->get('month')=='4' ?'selected="selected"':''}}>
                                        April
                                    </option>
                                    <option value="5" {{ request()->has('month') && request()->get('month')=='5' ?'selected="selected"':''}}>
                                        May
                                    </option>
                                    <option value="6" {{ request()->has('month') && request()->get('month')=='6' ?'selected="selected"':''}}>
                                        June
                                    </option>
                                    <option value="7" {{ request()->has('month') && request()->get('month')=='7' ?'selected="selected"':''}}>
                                        July
                                    </option>
                                    <option value="8" {{ request()->has('month') && request()->get('month')=='8' ?'selected="selected"':''}}>
                                        August
                                    </option>
                                    <option value="9" {{ request()->has('month') && request()->get('month')=='9' ?'selected="selected"':''}}>
                                        September
                                    </option>
                                    <option value="10" {{ request()->has('month') && request()->get('month')=='10' ?'selected="selected"':''}}>
                                        October
                                    </option>
                                    <option value="11" {{ request()->has('month') && request()->get('month')=='11' ?'selected="selected"':''}}>
                                        November
                                    </option>
                                    <option value="12" {{ request()->has('month') && request()->get('month')=='12' ?'selected="selected"':''}}>
                                        December
                                    </option>
                                </select>
                            </div>
                        </div>



                        <div class="col-sm-2">
                        <label for="" class="control-label">Select Status</label>
                            <div class="input-prepend input-group" >
                                <select class="form-control"  name="status">
                                    <option value="">Select Status</option>
                                    <option value="approved"  {{ request()->has('status') && request()->get('status')=='approved' ?'selected="selected"':''}}>Approved Booking</option>
                                    <option value="pending" {{ request()->has('status') && request()->get('status')=='pending' ?'selected="selected"':''}}>Pending Booking</option>
                                    <option value="cancelled" {{ request()->has('status') && request()->get('status')=='cancelled' ?'selected="selected"':''}}>Cancelled Booking</option>

                                </select>
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <label for="" class="control-label"></label>
                            <div class="input-prepend input-group">
                                <button type="submit"  class="btn btn-success btn-large">Search</button>
                            </div>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>

    <div class="panel">
        <div class="panel-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="tile-box bg-primary">
                            <div class="tile-header">
                                Total Bookings
                                <div class="float-right">

                                </div>
                            </div>
                            <div class="tile-content-wrapper">
                                <i class="glyph-icon icon-money"></i>
                                <div class="tile-content">
                                    <span></span>
                                    {{$totalBooking}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="example-box-wrapper">

                    <table id="datatable-fixedcolumns" class="table table-striped ">
                        <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Booked Service</th>
                                <th>Booked Date & Time</th>
                                <th>Booking Status</th>
                            </tr>
                        </thead>
                        <tbody id="result">
                        @if(!empty($bookings))
                            @foreach($bookings as $booking)
                                <tr>
                                    <th>{{$booking->customers->name}}</th>
                                    <th>{{$booking->treatment}}</th>
                                    <th>{{ formatDate($booking->time_slots,'M d Y H:i')}}</th>
                                    <th>{{$booking->status_text}}</th>
                                </tr>
                            @endforeach
                        </tbody>
                        @else
                            <tr>
                                <td colspan="4">No records founds</td>
                            </tr>
                        @endif
                </table>

                <div class="col-md-12 text-right">
                    @if(!empty($bookings))
                        {!! $bookings->appends($search_params)->render() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
@section('page-specific-scripts')
    <script type="text/javascript">
        /* Datepicker bootstrap */

        $(function () {
            "use strict";
            $('.bootstrap-datepicker').bsdatepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,

            });
            $('.bootstrap-datepicker').on('changeDate', function(ev){
                $(this).bsdatepicker('hide');
            });
        });

    </script>

@endsection
