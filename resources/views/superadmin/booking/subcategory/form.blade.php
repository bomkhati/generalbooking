{!! csrf_field() !!}

    <div class="form-group">
        <label class="col-sm-3 control-label">Select Category <span class="text-danger">*</span></label>
        <div class="col-sm-6">
            <select class="form-control" name="category_id" id="category_id">
                @foreach($categories as $category)
                    <option value="{{$category->id}}"
                    @if(isset($savedCategory))
                        {{$category->id==$savedCategory->id?'selected="selected"':''}}
                            @endif
                    >{{$category->category}}
                    </option>
                @endforeach;
            </select>
            <span class="validation-error">{{ $errors->first('category_id') }}</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Sub Category</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="sub_category"
                   value="{{ old('sub_category', isset($subcategory->sub_category) ? $subcategory->sub_category : '') }}"/>
            <span class="validation-error">{{ $errors->first('sub_category') }}</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Price</label>

        <div class="col-sm-6">
            <input type="text" class="form-control" name="price"
                   value="{{ old('price', isset($subcategory->price) ? $subcategory->price : '') }}"/>
            <span class="validation-error">{{ $errors->first('price') }}</span>
        </div>
    </div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('subcategory.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
