@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Booking Categories</h2>


    </div>

    <div class="panel">
        <div class="panel-body">

            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('category.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add Category Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($categories->count() > 0)
                    @foreach($categories as $k => $category)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $category->category }}</td>

                            <td>

                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">

                                        <li>
                                            <a href="{{ route('category.edit', [$category->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('category.destroy',
                                                [$category->id]) }}" title="">
                                                {!! delete_form(route('category.destroy',
                                                [$category->id]))
                                                 !!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>



                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>


        </div>
    </div>
@stop
