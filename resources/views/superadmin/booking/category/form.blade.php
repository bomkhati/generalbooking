{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Category</label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="category" placeholder="Category"
               value="{{ old('category', isset($category->category) ? $category->category : '') }}"/>
        <span class="validation-error">{{ $errors->first('category') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('category.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
