@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Customer Bookings</h2>


    </div>

    <div class="panel">
        <div class="panel-body">

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Booked By</th>
                    <th>Customer Type</th>
                    <th>Booked Time & Date</th>
                    <th>Booked Service</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($bookings->count() > 0)
                    @foreach($bookings as $k => $booking)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $booking->customers->name }}</td>
                            <td>{{!empty($booking->customers->customer_type)?ucfirst($booking->customers->customer_type):'NA'}}</td>
                            <td>{{ date('D F Y H:i A', strtotime($booking->time_slots)) }}</td>
                            <td>{{ $booking->treatment }}</td>
                            <td>
                                <div class="booking_status_text">
                                    @if($booking->status=='canceled')
                                        <span class="bs-badge badge-danger">  {{ $booking->status_text }}</span>
                                    @elseif($booking->status=='approved')
                                        <span class="bs-badge badge-success"> {{ $booking->status_text }}</span>
                                    @else
                                        <span class="bs-badge badge-warning"> {{ $booking->status_text }}</span>
                                    @endif
                                </div>
                                <div class="booking_status_form" style="display: none;">
                                    <select class="form-control status_form" onchange="updateForm(this, {{$booking->id}})">
                                        <option value="canceled" {{$booking->status=='canceled' ? 'selected="selected"' :''}}>Cancelled</option>
                                        <option value="approved" {{$booking->status=='approved' ? 'selected="selected"' :''}}>Approved</option>
                                        <option value="pending" {{$booking->status=='pending' ? 'selected="selected"' :''}}>Pending</option>
                                    </select>
                                </div>
                            </td>

                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('customer-booking.destroy',[$booking->id]) }}">
                                                {!! delete_form(route('customer-booking.destroy',[$booking->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $bookings->render() !!}
            </div>

        </div>
    </div>
    <script>
        function  updateForm(form, bookingId) {
            status = $(form).val();

            if(status=='approved'){
                html =  '<span class="bs-badge badge-success">  Approved </span>';
            }
            else if(status=='canceled'){
                html =  '<span class="bs-badge badge-danger">  Cancelled </span>';
            }
            else{
                html =  '<span class="bs-badge badge-primary">  Pending </span>';
            }

            token = "{{ csrf_token() }}";
            $.ajax
            ({
                type: "get",
                url: '{{route('superadmin.ajax.booking.status')}}',

                data: "status="+status+"&bookingId="+bookingId+"&_token="+token,
                success: function(result)
                {
                    $(form).parent('.booking_status_form').hide();
                    $(form).parent('.booking_status_form').prev('.booking_status_text').html(html);
                }
            });
        }
        $(document).ready(function () {
            $('.booking_status_text').click(function () {
                var html = $.trim($(this).find('span').html());
                var compare = "Approved";
                if(html.toLowerCase() != compare.toLowerCase()) {
                    $(this).next('.booking_status_form').toggle();
                }
            })


        })
    </script>
@stop
