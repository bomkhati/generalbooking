{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Day</label>
    <div class="col-sm-6">
        <input type="date" name="day" id="day" value="{{ old('day', isset($generalSetting->day) ? $generalSetting->day : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('day') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Opening Time</label>
    <div class="col-sm-6">
        <input type="time" name="start_time" id="start_time" value="{{ old('start_time', isset($generalSetting->start_time) ? $generalSetting->start_time : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('start_time') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Closing Time</label>
    <div class="col-sm-6">
        <input type="time" name="end_time" id="end_time" value="{{ old('end_time', isset($facility->end_time) ? $facility->end_time : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('end_time') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Total Booking</label>
    <div class="col-sm-6">
        <input type="text" name="total_booking" id="total_booking" value="{{ old('total_booking', isset($generalSetting->total_booking) ? $generalSetting->total_booking : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('total_booking') }}</i>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Booking Interval (Min)</label>
    <div class="col-sm-6">
        <input type="text" name="booking_interval" id="booking_interval" value="{{ old('booking_interval', isset($generalSetting->booking_interval) ? $generalSetting->booking_interval : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('booking_interval') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Space Available</label>
    <div class="col-sm-6">
        <input type="text" name="space_available" id="space_available" value="{{ old('space_available', isset($generalSetting->space_available) ? $generalSetting->space_available : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('space_available') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">IsHoliday</label>
    <div class="col-sm-6">
        <input type="radio" name="isHoliday" value="yes" class="radio-inline"
                {{ old('isHoliday', isset($user->isHoliday) ? $user->isHoliday : '')=='yes'?'checked="checked"':''}}> Yes
        <input type="radio" name="isHoliday" value="no" class="radio-inline"
                {{ old('isHoliday', isset($user->isHoliday) ? $user->isHoliday : '')=='no'?'checked="checked"':''}}> No
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('default-setting.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
