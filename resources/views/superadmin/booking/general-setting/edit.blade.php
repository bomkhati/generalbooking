@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Edit General Booking Settings</h2>


    </div>

    <div class="panel">
        <div class="panel-body">
            <h3 class="title-hero">
                Basic Information
            </h3>

            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" method="POST" action="{{ route('general-setting.update',[$generalSetting->id]) }}"
                      enctype="multipart/form-data">
                    {{ method_field('patch') }}
                    @include('superadmin.booking.general-setting.form')
                </form>
            </div>
        </div>
    </div>

@stop