@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Special Booking Settings</h2>


    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('general-setting.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i>Add General Booking Settings Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Day</th>
                    <th>Opening Time</th>
                    <th>Closing Time</th>
                    <th>Total Booking</th>
                    <th>Space Available</th>
                    <th>Booking Interval</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($generalSettings->count() > 0)
                    @foreach($generalSettings as $k => $generalSetting)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $generalSetting->day }}</td>
                            <td>{{ $generalSetting->start_time }}</td>
                            <td>{{ $generalSetting->end_time }}</td>
                            <td>{{ $generalSetting->total_booking }}</td>
                            <td>{{ $generalSetting->space_available }}</td>
                            <td>{{ $generalSetting->booking_interval }}</td>

                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('general-setting.edit', [$generalSetting->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('general-setting.destroy',[$generalSetting->id]) }}">
                                                {!! delete_form(route('general-setting.destroy',[$generalSetting->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $generalSettings->render() !!}
            </div>

        </div>
    </div>
@stop
