@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Update Page</h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" method="POST" action="{{ route('page.update', $page->id) }}"
                      enctype="multipart/form-data">
                    {{ method_field('patch') }}
                    @include('superadmin.page.form')
                </form>
            </div>
        </div>
    </div>

@stop