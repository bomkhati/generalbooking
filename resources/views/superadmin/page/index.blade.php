@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Manage Page</h2>
    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            <a class="btn btn-sm btn-success add-button" href="{{ route('page.create') }}">
                <i class="fa fa-aw fc-agenda-axis"></i> Add CMS Page Here
            </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Menu Position</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $k => $page)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{$page->title }}</td>
                        <td><img src="{{ asset($page->thumbnail_path) }}" height="80px;" class="border" /></td>
                        <td>{{$page->menu_position }}</td>
                        <td>{{$page->status_text}}</td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('page.edit', [$page->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('page.destroy',[$page->id]) }}" title="">
                                            {!! delete_form(route('page.destroy', [$page->id]))!!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $pages->render() !!}
            </div>
        </div>
    </div>
@stop

