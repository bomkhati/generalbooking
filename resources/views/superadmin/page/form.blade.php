{!! csrf_field() !!}
<div class="clearfix"></div>
<div class="form-group">
    <label class="col-sm-3 control-label">Title</label>
    <div class="col-sm-6">
        <input type="text" name="title" id="title" value="{{ old('title', isset($page->title) ? $page->title : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('title') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>

    <div class="col-sm-9">
         <textarea class="ckeditor" cols="80" id="editor1" name="description" rows="10" placeholder="Description">
            {{ old('description', isset($page->description) ? $page->description : '') }}
            </textarea>
        <span class="validation-error">{{ $errors->first('$page') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Featured Image</label>
    <div class="col-sm-6">
        @if(isset($page->image))
            <img src="{{ asset($page->thumbnail_path) }}" height="100px" alt=""/>
            <br/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Menu Position</label>
    <div class="col-sm-6">
        <input type="number" id="menu_position" name="menu_position" value="{{ old('menu_position', isset($page->menu_position) ? $page->menu_position : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('menu_position') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>
    <div class="col-sm-6" >
        <input type="radio" name="status" value="active" class="radio-inline" @if(empty($page->status)) checked="checked" @endif
                {{old('status',isset($page->status)?$page->status:'')=='active'?'checked="checked"':''}}>  Active
        <input type="radio" name="status" class="api" value="in_active" class="radio-inline"
                {{old('status',isset($page->status)?$page->status:'')=='in_active'?'checked="checked"':''}} >  Inactive
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-6">
        <input type="submit" name="" value="Save" class="btn btn-success">
        <a href="{{ route('page.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>
