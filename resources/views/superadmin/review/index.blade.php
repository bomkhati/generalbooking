@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Customer Reviews</h2>
    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('review.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add Customer Review
                </a>
            </div>
            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Customer Name</th>
                    <th>Review</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($reviews->count() > 0)
                    @foreach($reviews as $k => $review)
                        <tr>
                            <td>{{ ++$k }}</td>
                            @php
                                $user = $review->user_id;
                                $name = $review->full_name;
                            @endphp
                            @if($user != null)
                                <td>{{ $review->customers->name }} </td>
                            @endif
                            @if($name != null)
                                <td>{{ $review->full_name }} </td>
                            @endif
                            <td>{!! $review->review !!}</td>
                            <td>
                                <div class="review_status_text">
                                    @if($review->status=='approved')
                                        <span class="bs-badge badge-success">  {{ $review->status_text }}</span>
                                    @else
                                        <span class="bs-badge badge-warning"> {{ $review->status_text }}</span>
                                    @endif
                                </div>
                                <div class="review_status_text" style="display: none;">
                                    <select class="form-control status_form" onchange="updateForm(this, {{$review->id}})">
                                        <option value="approved" {{$review->status=='approved' ? 'selected="selected"' :''}}>Approved</option>
                                        <option value="pending" {{$review->status=='pending' ? 'selected="selected"' :''}}>Pending</option>
                                    </select>
                                </div>
                            </td>

                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        @if($name != null)
                                            <li>
                                                <a href="{{ route('review.edit', [$review->id]) }}" title="">
                                                    <i class="glyph-icon icon-pencil mrg5R"></i>
                                                    Edit
                                                </a>
                                            </li>
                                        @endif

                                        <li>
                                            <a href="{{ route('review.destroy',[$review->id]) }}">
                                                {!! delete_form(route('review.destroy',[$review->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $reviews->render() !!}
            </div>

        </div>
    </div>
    <script>
        function  updateForm(form, reviewId) {
            status = $(form).val();

            if(status=='approved'){
                html =  '<span class="bs-badge badge-success">  Approved </span>';
            }
            else{
                html =  '<span class="bs-badge badge-primary">  Pending </span>';
            }

            token = "{{ csrf_token() }}";
            $.ajax
            ({
                type: "get",
                url: '{{route('superadmin.ajax.review.status')}}',

                data: "status="+status+"&reviewId="+reviewId+"&_token="+token,
                success: function(result)
                {
                    $(form).parent('.review_status_text').hide();
                    $(form).parent('.review_status_text').prev('.review_status_text').html(html);
                }
            });
        }
        $(document).ready(function () {
            $('.review_status_text').click(function () {
                var html = $.trim($(this).find('span').html());
                var compare = "Approved";
                if(html.toLowerCase() != compare.toLowerCase()) {
                    $(this).next('.review_status_text').toggle();
                }
            })


        })
    </script>
@stop
