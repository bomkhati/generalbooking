{!! csrf_field() !!}
<div class="clearfix"></div>

<div class="form-group">
    <label class="col-sm-3 control-label">Customer Name</label>
    <div class="col-sm-6">
        <input type="text" name="full_name" id="full_name" value="{{ old('full_name', isset($review->full_name) ? $review->full_name : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('full_name') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Review</label>
    <div class="col-sm-9">
         <textarea class="ckeditor" cols="80" id="editor1" name="review" rows="6" placeholder="Write Review">
            {{ old('review', isset($review->review) ? $review->review : '') }}
         </textarea>
        <span class="validation-error">{{ $errors->first('review') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-6">
        <input type="submit" name="" value="Save" class="btn btn-success">
        <a href="{{ route('review.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>
