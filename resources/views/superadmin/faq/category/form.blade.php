{!! csrf_field() !!}



<div class="form-group">
    <label class="col-sm-3 control-label">Name</label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="title" placeholder="Title"
               value="{{ old('title', isset($category->title) ? $category->title : '') }}"/>
        <span class="validation-error">{{ $errors->first('title') }}</span>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>

    <div class="col-sm-9">
         <textarea class="form-control" name="description"  placeholder="Description">{{ old('description', isset($category->description) ? $category->description : '') }}</textarea>
        <span class="validation-error">{{ $errors->first('description') }}</span>
    </div>
</div>






<div class="form-group">
    <label class="col-sm-3 control-label">Status <span class="text-danger">*</span></label>

    <div class="col-sm-6">
        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-medium bootstrap-switch-animate">
            <div class="bootstrap-switch-container">
                <input data-on-color="primary" name="status"
                       {{ old('status', isset($category->status) ? $category->status : '') == 'active' ? 'checked' : '' }}
                       @if(empty($category->status))
                       checked
                       @endif
                       class="input-switch" data-size="medium"
                       data-on-text="Active" data-off-text="Inactive"
                       type="checkbox"></div>

        </div>
        <span class="validation-error">{{ $errors->first('status') }}</span>
    </div>

</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('faq-category.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
