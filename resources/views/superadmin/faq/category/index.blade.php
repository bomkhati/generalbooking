@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>FAQ Categories</h2>


    </div>

    <div class="panel">
        <div class="panel-body">

            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('faq-category.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add FAQ Category Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($categories->count() > 0)
                    @foreach($categories as $k => $category)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $category->title }}</td>
                            <td>{{ $category->status_text }}</td>

                            <td>

                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">

                                        <li>
                                            <a href="{{ route('faq-category.edit', [$category->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('faq-category.destroy',
                                                [$category->id]) }}" title="">
                                                {!! delete_form(route('faq-category.destroy',
                                                [$category->id]))
                                                 !!}
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('faq.create') }}" title="">
                                                <i class="glyph-icon icon-plus mrg5R"></i>
                                                Add FAQ
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ route('faq.index') }}" title="">
                                                <i class="glyph-icon icon-th-list mrg5R"></i>
                                                Manage FAQ
                                            </a>
                                        </li>



                                    </ul>
                                </div>



                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>


        </div>
    </div>
@stop
