@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Faq</h2>


    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('faq.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add FAQ Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">

                <thead>
                <tr>
                    <th>SN</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($faqs->count() > 0)
                    @foreach($faqs as $k => $faq)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $faq->faq_catecory->title }}</td>
                            <td>{{ $faq->title }}</td>
                            <td>{{ $faq->status_text }}</td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('faq.edit', [$faq->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('faq.destroy',
                                                [$faq->id]) }}" title="">
                                                {!! delete_form(route('faq.destroy',
                                                [$faq->id]))
                                                !!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $faqs->render() !!}
            </div>
        </div>
    </div>
@stop