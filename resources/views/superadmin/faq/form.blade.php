{!! csrf_field() !!}



<div class="form-group">
    <label class="col-sm-3 control-label">Title</label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="title" placeholder="Title"
               value="{{ old('title', isset($faq->title) ? $faq->title : '') }}"/>
        <span class="validation-error">{{ $errors->first('title') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">FAQ Category <span class="text-danger">*</span></label>

    <div class="col-sm-6">
        <select class="form-control" name="faq_category_id" id="faq_category_id">

                @foreach($faqCategories as $category)
                <option value="{{$category->id}}"
                @if(isset($savedCategory))
                    {{$category->id==$savedCategory->id?'selected="selected"':''}}
                        @endif
                >{{$category->title}}
                </option>
                   @endforeach;


        </select>
        <span class="validation-error">{{ $errors->first('faq_category_id') }}</span>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>

    <div class="col-sm-9">
         <textarea class="ckeditor" cols="80" id="editor1" name="description" rows="10" placeholder="Description">
            {{ old('description', isset($faq->description) ? $faq->description : '') }}
            </textarea>
        <span class="validation-error">{{ $errors->first('description') }}</span>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label">Status <span class="text-danger">*</span></label>
    <div class="col-sm-6">
        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-medium bootstrap-switch-animate">
            <div class="bootstrap-switch-container">
                <input data-on-color="primary" name="status"
                       {{ old('status', isset($faq->status) ? $faq->status : '') == 'active' ? 'checked' : '' }}
                       @if(empty($faq->status))
                       checked
                       @endif
                       class="input-switch" data-size="medium"
                       data-on-text="Active" data-off-text="Inactive"
                       type="checkbox"></div>
        </div>
        <span class="validation-error">{{ $errors->first('status') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('faq.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
