@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Manage Facility</h2>
    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            <a class="btn btn-sm btn-success add-button" href="{{ route('facility.create') }}">
                <i class="fa fa-aw fc-agenda-axis"></i> Add Facility Here
            </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Facility</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($facilities as $k => $facility)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{$facility->title }}</td>
                        <td><img src="{{ asset($facility->thumbnail_path) }}" height="60px;" class="border" /></td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('facility.edit', [$facility->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('facility.destroy',[$facility->id]) }}" title="">
                                            {!! delete_form(route('facility.destroy', [$facility->id]))!!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $facilities->render() !!}
            </div>
        </div>
    </div>
@stop

