{!! csrf_field() !!}
<div class="clearfix"></div>
<div class="form-group">
    <label class="col-sm-3 control-label">Facility</label>
    <div class="col-sm-6">
        <input type="text" name="title" id="title" value="{{ old('title', isset($facility->title) ? $facility->title : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('title') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>

    <div class="col-sm-9">
         <textarea class="ckeditor" cols="80" id="editor1" name="description" rows="10" placeholder="Description">
            {{ old('description', isset($facility->description) ? $facility->description : '') }}
            </textarea>
        <span class="validation-error">{{ $errors->first('$facility') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Upload Image</label>
    <div class="col-sm-6">
        @if(isset($facility->file))
            <img src="{{ asset($facility->thumbnail_path) }}" height="100px" alt=""/>
            <br/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>
    <div class="col-sm-6" >
        <input type="radio" name="status" value="active" class="radio-inline" @if(empty($facility->status)) checked="checked" @endif
                {{old('status',isset($facility->status)?$facility->status:'')=='active'?'checked="checked"':''}}>  Active
        <input type="radio" name="status" class="api" value="in_active" class="radio-inline"
                {{old('status',isset($facility->status)?$facility->status:'')=='in_active'?'checked="checked"':''}} >  Inactive
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-6">
        <input type="submit" name="" value="Save" class="btn btn-success">
        <a href="{{ route('facility.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>
