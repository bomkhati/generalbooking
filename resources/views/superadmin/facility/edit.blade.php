@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Update Facility</h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" method="POST" action="{{ route('facility.update', $facility->id) }}"
                      enctype="multipart/form-data">
                    {{ method_field('patch') }}
                    @include('superadmin.facility.form')
                </form>
            </div>
        </div>
    </div>

@stop