@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Add New Facility </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" id="cmsPageForm" method="POST" action="{{ route('facility.store') }}"
                      enctype="multipart/form-data">
                    @include('superadmin.facility.form')
                </form>
            </div>
        </div>
    </div>

@stop