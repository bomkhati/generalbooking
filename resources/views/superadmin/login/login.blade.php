<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Booking System </title>
    <link rel="icon" href="{{ asset('storage/frontweb/favicon.png') }}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('storage/frontweb/favicon.png') }}" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('storage/superadmin/images/icons/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('storage/superadmin/images/icons/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('storage/superadmin/images/icons/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('storage/superadmin/images/icons/apple-touch-icon-57-precomposed.png')}}">
    <link rel="shortcut icon" href="{{ asset('storage/superadmin/chattels/image-storage/superadmin/ameb_logo_small.png')}}">



    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/bootstrap/css/bootstrap.css')}}">


    <!-- HELPERS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/backgrounds.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/boilerplate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/border-radius.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/page-transitions.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/spacing.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/typography.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/utils.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/colors.css')}}">

    <!-- ELEMENTS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/badges.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/buttons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/content-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/dashboard-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/forms.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/images.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/info-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/invoice.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/loading-indicators.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/menus.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/panel-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/response-messages.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/responsive-tables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/ribbon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/social-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/tables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/tile-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/elements/timeline.css')}}">



    <!-- ICONS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/icons/fontawesome/fontawesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/icons/linecons/linecons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/icons/spinnericon/spinnericon.css')}}">


    <!-- WIDGETS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/accordion-ui/accordion.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/calendar/calendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/carousel/carousel.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/charts/justgage/justgage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/charts/morris/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/charts/piegage/piegage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/charts/xcharts/xcharts.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/chosen/chosen.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/colorpicker/colorpicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/datatable/datatable.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/datepicker/datepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/datepicker-ui/datepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/dialog/dialog.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/dropdown/dropdown.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/dropzone/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/file-input/fileinput.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/input-switch/inputswitch.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/input-switch/inputswitch-alt.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/ionrangeslider/ionrangeslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/jcrop/jcrop.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/jgrowl-notifications/jgrowl.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/loading-bar/loadingbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/maps/vector-maps/vectormaps.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/markdown/markdown.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/modal/modal.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/multi-select/multiselect.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/multi-upload/fileupload.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/nestable/nestable.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/noty-notifications/noty.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/popover/popover.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/pretty-photo/prettyphoto.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/progressbar/progressbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/range-slider/rangeslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/slidebars/slidebars.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/slider-ui/slider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/summernote-wysiwyg/summernote-wysiwyg.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/tabs-ui/tabs.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/theme-switcher/themeswitcher.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/timepicker/timepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/tocify/tocify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/tooltip/tooltip.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/touchspin/touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/uniform/uniform.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/wizard/wizard.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/widgets/xeditable/xeditable.css')}}">

    <!-- SNIPPETS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/chat.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/files-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/login-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/notification-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/progress-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/todo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/user-profile.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/snippets/mobile-navigation.css')}}">

    <!-- APPLICATIONS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/applications/mailbox.css')}}')}}">

    <!-- Admin theme -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/themes/admin/layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/themes/admin/color-schemes/default.css')}}">

    <!-- Components theme -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/themes/components/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/themes/components/border-radius.css')}}">

    <!-- Admin responsive -->

    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/responsive-elements.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('storage/superadmin/helpers/admin-responsive.css')}}">

    <!-- JS Core -->

    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-core.js')}}"></script>
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-ui-core.js')}}"></script>
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-ui-widget.js')}}"></script>
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-ui-mouse.js')}}"></script>
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-ui-position.js')}}"></script>
    <!--<script type="text/javascript" src="{{ asset('storage/superadmin/js-core/transition.js')}}"></script>-->
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/modernizr.js')}}"></script>
    <script type="text/javascript" src="{{ asset('storage/superadmin/js-core/jquery-cookie.js')}}"></script>





    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>
<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<style type="text/css">

    html,body {
        height: 100%;
        background: #bbc1bf;
    }

</style>

<div class="center-vertical pt-5">
    <div class="center-content row">

        <div class="col-md-8 col-lg-6 clearfix center-margin">
            <div class="col-md-8 col-md-offset-2 text-center">
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <form class="form-horizontal" method="POST" action="{{ route('superadmin.login') }}">
                        {{ csrf_field() }}
                        <div class="content-box">
                            <h3 class="content-box-header content-box-header-alt bg-default">
                                <span class="icon-separator">
                                    <i class="glyph-icon icon-cog"></i>
                                </span>
                                <span class="header-wrapper">
                                  Booking System Admin Panel
                                </span>

                            </h3>

                            <div class="content-box-wrapper">
                                <div class="form-group{{ $errors->has('email_or_username') ? ' has-error' : '' }}">
                                    <div class="input-group" style="padding: 0 1.5rem 0 1.5rem">
                                        <input id="email_or_username" type="text" placeholder="Your Username or Email" class="form-control" name="email_or_username" value="{{ old('email') }}" required autofocus>

                                        <span class="input-group-addon bg-blue">
                                            <i class="glyph-icon icon-envelope-o"></i>
                                        </span>

                                    </div>
                                </div>
                                <span class="help-block">
                                    <p style="color:red">{{session('error') }}</p>
                                </span>
                                @if ($errors->has('email_or_username'))
                                    <span class="help-block">
                                        <p style="color:red">{{ $errors->first('email_or_username') }}</p>
                                    </span>
                                @endif
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="input-group" style="padding: 0 1.5rem 0 1.5rem">
                                        <input id="password" type="password" placeholder="Your Password" class="form-control" name="password" required>

                                        <span class="input-group-addon bg-blue">
                                            <i class="glyph-icon icon-unlock-alt"></i>
                                        </span>

                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <p style="color:red">{{ $errors->first('password') }}</p>
                                    </span>
                                @endif
                                <small>If you have forgotten your password, please contact Technical Staff</small>
                                <div class="form-group">
                                    <!--  <a href="" title="Recover password">Forgot Your Password?</a> -->
                                </div>
                                <button type="submit" class="btn btn-success btn-block">Login to Your Account</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>



<!-- WIDGETS -->

<script type="text/javascript" src="{{ asset('storage/superadmin/bootstrap/js/bootstrap.js')}}"></script>

<!-- Bootstrap Dropdown -->

<!-- <script type="text/javascript" src="{{ asset('storage/superadmin/widgets/dropdown/dropdown.js')}}"></script> -->

<!-- Bootstrap Tooltip -->

<!-- <script type="text/javascript" src="{{ asset('storage/superadmin/widgets/tooltip/tooltip.js')}}"></script> -->

<!-- Bootstrap Popover -->

<!-- <script type="text/javascript" src="{{ asset('storage/superadmin/widgets/popover/popover.js')}}"></script> -->

<!-- Bootstrap Progress Bar -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/progressbar/progressbar.js')}}"></script>

<!-- Bootstrap Buttons -->

<!-- <script type="text/javascript" src="{{ asset('storage/superadmin/widgets/button/button.js')}}"></script> -->

<!-- Bootstrap Collapse -->

<!-- <script type="text/javascript" src="{{ asset('storage/superadmin/widgets/collapse/collapse.js')}}"></script> -->

<!-- Superclick -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/superclick/superclick.js')}}"></script>

<!-- Input switch alternate -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/input-switch/inputswitch-alt.js')}}"></script>

<!-- Slim scroll -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/slimscroll/slimscroll.js')}}"></script>

<!-- Slidebars -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/slidebars/slidebars.js')}}"></script>
<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/slidebars/slidebars-demo.js')}}"></script>

<!-- PieGage -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/charts/piegage/piegage.js')}}"></script>
<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/charts/piegage/piegage-demo.js')}}"></script>

<!-- Screenfull -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/screenfull/screenfull.js')}}"></script>

<!-- Content box -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/content-box/contentbox.js')}}"></script>

<!-- Overlay -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/overlay/overlay.js')}}"></script>

<!-- Widgets init for demo -->

<script type="text/javascript" src="{{ asset('storage/superadmin/js-init/widgets-init.js')}}"></script>

<!-- Theme layout -->

<script type="text/javascript" src="{{ asset('storage/superadmin/themes/admin/layout.js')}}"></script>

<!-- Theme switcher -->

<script type="text/javascript" src="{{ asset('storage/superadmin/widgets/theme-switcher/themeswitcher.js')}}"></script>

</body>
</html>