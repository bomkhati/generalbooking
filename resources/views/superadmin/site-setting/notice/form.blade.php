{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Title</label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="title" placeholder="Title"
               value="{{ old('title', isset($notice->title) ? $notice->title : '') }}"/>
        <span class="validation-error">{{ $errors->first('title') }}</span>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label">Notice</label>

    <div class="col-sm-9">
         <textarea class="ckeditor" cols="80" id="editor1" name="notice" rows="10" placeholder="Description">
            {{ old('notice', isset($notice->notice) ? $notice->notice : '') }}
            </textarea>
        <span class="validation-error">{{ $errors->first('notice') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>

    <div class="col-sm-6">
        <input type="radio" class="radio-inline" name="status" value="active"   {{ old('status', isset($notice->status) ? $notice->status : '') == 'active' ? 'checked="checked"' : '' }}> Active
        <input type="radio" class="radio-inline" name="status"  value="in_active"  {{ old('status', isset($notice->status) ? $notice->status : '') == 'in_active' ? 'checked="checked"' : '' }}> In Active

        <span class="validation-error">{{ $errors->first('status') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('site-notice.index') }}" class="btn btn-default btn-sm">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>
