@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Site Notices</h2>
    </div>

    <div class="panel">
        <div class="panel-body">
            @if($notices->count() > 1)
            <a class="btn btn-sm btn-success add-button" href="{{ route('site-notice.create') }}"><i class="fa fa-aw fc-agenda-axis"></i> Add Site Notice Here</a>
            <div class="clearfix"></div>
            @endif
            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($notices->count() > 0)
                    @foreach($notices as $k => $notice)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $notice->title }}</td>
                            <td>{{ $notice->status_text }}</td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('site-notice.edit', [$notice->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('site-notice.destroy',[$notice->id]) }}">
                                                {!! delete_form(route('site-notice.destroy',[$notice->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>


        </div>
    </div>
@stop
