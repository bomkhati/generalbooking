@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Add Footer Settings </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" id="" method="POST" action="{{ route('footer-setting.store') }}"
                      enctype="multipart/form-data">
                    @include('superadmin.site-setting.footer.form')
                </form>
            </div>
        </div>
    </div>

@stop