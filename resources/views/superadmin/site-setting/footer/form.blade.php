{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Location/Address</label>
    <div class="col-sm-9">
        <textarea class="ckeditor" cols="80" id="editor1" name="location" rows="6">
            {{ old('location', isset($footerSetting->location) ? $footerSetting->location : '') }}
        </textarea>
        <span class="validation-error">{{ $errors->first('$footerSetting') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Opening Hours</label>
    <div class="col-sm-9">
        <textarea class="ckeditor" cols="80" id="editor1" name="opening_hours" rows="6">
            {{ old('opening_hours', isset($footerSetting->opening_hours) ? $footerSetting->opening_hours : '') }}
        </textarea>
        <span class="validation-error">{{ $errors->first('$footerSetting') }}</span>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Copyright</label>
    <div class="col-sm-6">
        <input type="text" name="copyright" value="{{ old('copyright', isset($footerSetting->copyright) ? $footerSetting->copyright : '') }}" placeholder="Copyright" class="form-control">
        <i class="errormsg">{{ $errors->first('copyright') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Best Award</label>

    <div class="col-sm-6">
        @if(isset($footerSetting->file))
            <img src="{{ asset($footerSetting->thumbnail_path) }}" height="40px" alt=""/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>
    <div class="col-sm-6" >
        <input type="radio" name="status" value="active" class="radio-inline"
                {{old('status',isset($footerSetting->status)?$footerSetting->status:'')=='active'?'checked="checked"':''}}>  Active
        <input type="radio" name="status" class="api" value="in_active" class="radio-inline"
                {{old('status',isset($footerSetting->status)?$footerSetting->status:'')=='in_active'?'checked="checked"':''}} >  Inactive
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-8">
        <button type="submit" class="btn btn-success btn-sm-abs">Save Footer Setting</button>
        <a href="{{ route('footer-setting.index') }}" class="btn btn-default btn-sm-abs">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>


