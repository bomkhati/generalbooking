@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Footer Setting </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            @if($footerSettings->count()<1)
                <div class="panel-heading text-right">
                    <a class="btn btn-sm btn-success add-button" href="{{ route('footer-setting.create') }}">
                        <i class="fa fa-aw fc-agenda-axis"></i> Add Footer Setting Here
                    </a>
                </div>
            @endif
            <div class="clearfix"></div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Copy Right</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($footerSettings->count() > 0)
                    @foreach($footerSettings as $k => $footerSetting)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $footerSetting->copyright }}</td>
                            <td><img src="{{ asset($footerSetting->thumbnail_path) }}" height="40px;" class="border" /></td>
                            <td>{{ $footerSetting->status_text }}</td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('footer-setting.edit', [$footerSetting->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('footer-setting.destroy',[$footerSetting->id]) }}" title="">
                                                {!! delete_form(route('footer-setting.destroy', [$footerSetting->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
