@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Site Maintenance Mode</h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($maintenances->count() > 0)
                    @foreach($maintenances as $k => $maintenance)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ $maintenance->title }}</td>
                            <td>{{ $maintenance->status_text }}</td>

                            <td>
                                <a data-toggle="modal" data-target="#myModal">Change</a>

                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form class="form-horizontal bordered-row" method="POST" action="{{ route('site-maintenance.update',[$maintenance->id]) }}"
                  enctype="multipart/form-data">
                {{ method_field('patch') }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Maintenace Mode</h4>
                    </div>
                    <div class="modal-body">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Maintainance Mode</label>

                            <div class="col-sm-6">
                                <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-medium bootstrap-switch-animate">
                                    <div class="bootstrap-switch-container">
                                        <input data-on-color="primary" name="status"
                                               {{ old('state_price', isset($maintenance->status) ? $maintenance->status : '') == 'active' ? 'checked' : '' }}
                                               class="input-switch" data-size="medium"
                                               data-on-text="Enabled" data-off-text="Disabled"
                                               type="checkbox"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Your Password <span class="text-danger">*</span></label>

                            <div class="col-sm-6">
                                <input type="password" class="form-control" required name="password" placeholder="Password"/>
                                <span class="validation-error">{{ $errors->first('password') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
