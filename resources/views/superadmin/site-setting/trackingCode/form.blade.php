{!! csrf_field() !!}
<div class="form-group">
    <label class="col-sm-2 control-label">SEO Settings</label>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="keyword" class="md-textarea form-control" placeholder="Include Keywords" rows="4">{{ old('keyword', isset($trackingCode->keyword) ? $trackingCode->keyword: '') }}</textarea>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="keyword_description" class="md-textarea form-control" placeholder="Include Keywords description" rows="4">{{ old('keyword_description', isset($trackingCode->keyword_description) ? $trackingCode->keyword_description: '') }}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Facebook Pixel</label>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="facebook_script" class="md-textarea form-control" placeholder="Include code excluding script tag" rows="4">{{ old('facebook_script', isset($trackingCode->facebook_script) ? $trackingCode->facebook_script: '') }}</textarea>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="facebook_noscript" class="md-textarea form-control" placeholder="Include code excluding noscript tag" rows="4">{{ old('facebook_noscript', isset($trackingCode->facebook_noscript) ? $trackingCode->facebook_noscript: '') }}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Google Tag Manager</label>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="google_analytics_script" class="md-textarea form-control" placeholder="Include code excluding script tag " rows="4">{{ old('google_analytics_script', isset($trackingCode->google_analytics_script) ? $trackingCode->google_analytics_script : '') }}</textarea>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="md-form">
            <textarea name="google_analytics_noscript" class="md-textarea form-control" placeholder="Include code excluding noscript tag " rows="4">{{ old('google_analytics_noscript', isset($trackingCode->google_analytics_noscript) ? $trackingCode->google_analytics_noscript: '') }}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-8">
        <button type="submit" class="btn btn-success btn-sm-abs">Save Setting</button>
        <a href="{{ route('tracking-code.index') }}" class="btn btn-default btn-sm-abs">
            <i class="fa fa-aw fa-reply-o"></i> Back
        </a>
    </div>
</div>


