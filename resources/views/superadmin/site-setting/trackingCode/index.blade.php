@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Website Tracking Code Setting </h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            @if($trackingCodes->count()<1)
                <div class="panel-heading text-right">
                    <a class="btn btn-sm btn-success add-button" href="{{ route('tracking-code.create') }}">
                        <i class="fa fa-aw fc-agenda-axis"></i> Add Tracking Code Setting Here
                    </a>
                </div>
            @endif
            <div class="clearfix"></div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>SEO Tags</th>
                    <th>Facebook Pixel</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($trackingCodes->count() > 0)
                    @foreach($trackingCodes as $k => $trackingCode)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>
                                <textarea class="md-textarea form-control" rows="4">{!! $trackingCode->keyword !!}</textarea>&nbsp;
                                <textarea class="md-textarea form-control" rows="4">{!! $trackingCode->keyword_description !!}</textarea>
                            </td>
                            <td>
                                <textarea class="md-textarea form-control" rows="4">{!! $trackingCode->facebook_script !!}</textarea>&nbsp;
                                <textarea class="md-textarea form-control" rows="4">{!! $trackingCode->facebook_noscript !!}</textarea>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                        <i class="glyph-icon icon-cog"></i>
                                        <i class="glyph-icon icon-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <li>
                                            <a href="{{ route('tracking-code.edit', [$trackingCode->id]) }}" title="">
                                                <i class="glyph-icon icon-pencil mrg5R"></i>
                                                Edit
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('tracking-code.destroy',[$trackingCode->id]) }}" title="">
                                                {!! delete_form(route('tracking-code.destroy', [$trackingCode->id]))!!}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
