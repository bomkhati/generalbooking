@extends('layouts.superadmin')

@section('content')
    <div id="page-title">
        <h2>Update Website Tracking Code Setting</h2>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" method="POST" action="{{ route('tracking-code.update', $trackingCode->id) }}"
                      enctype="multipart/form-data">
                    {{ method_field('patch') }}
                    @include('superadmin.site-setting.trackingCode.form')
                </form>
            </div>
        </div>
    </div>

@stop