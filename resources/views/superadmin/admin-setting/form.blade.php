{{ csrf_field() }}

<div class="form-group">
    <label class="col-sm-3 control-label">Full Name</label>
    <div class="col-sm-6">
        <input type="text" name="name" class="form-control" value="{{ old('name', isset($user->name) ? $user->name : '') }}">
    </div>
    @if ($errors->has('name'))
        <span class="validation-error">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Phone</label>
    <div class="col-sm-6">
        <input type="text" name="phone" class="form-control" value="{{ old('phone', isset($user->phone) ? $user->phone : '') }}">
    </div>
    @if ($errors->has('phone'))
        <span class="validation-error">
        <strong>{{ $errors->first('phone') }}</strong>
    </span>
    @endif
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Email</label>
    <div class="col-sm-6">
        <input type="text" name="email" class="form-control" value="{{ old('email', isset($user->email) ? $user->email : '') }}">
    </div>
    @if ($errors->has('email'))
        <span class="validation-error">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status</label>
    <div class="col-sm-6">
        <input type="radio" name="status" value="active" class="radio-inline"
               {{ old('status', isset($user->status) ? $user->status : '')=='active'?'checked="checked"':''}}> Active
        <input type="radio" name="status" value="in_active" class="radio-inline"
                {{ old('status', isset($user->status) ? $user->status : '')=='in_active'?'checked="checked"':''}}> Inactive
    </div>
</div>
@if ($errors->has('status'))
    <span class="validation-error">
        <strong>{{ $errors->first('status') }}</strong>
    </span>
@endif
<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-6">
        <input type="submit" name="" value="Update" class="btn btn-success">
    </div>
</div>