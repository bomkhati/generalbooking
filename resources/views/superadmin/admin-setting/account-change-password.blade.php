<div class="modal fade" id="password-change-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-height: 200px !important;">
        <form class="form-horizontal change-pin-form" method="post" action="{{ route('account-profile.update.password', $accountHolder->id) }}">
            {{ method_field('patch') }}
            <div class="modal-content" style="max-height: 300px !important;">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Generate New Password</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to request a new password for the {{ $accountHolder->full_name }}?</p>

                    <div class="result-message-div" style="margin-top: 20px;">
                        <p>Email will be sent to account holder with the new password.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-sm ra-100">Generate New Password</button>
                    <button type="button" class="btn btn-purple btn-sm ra-100" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>