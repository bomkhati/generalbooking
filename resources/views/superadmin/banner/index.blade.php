@extends('layouts.superadmin')

@section('content')

    <div id="page-title">
        <h2>Banner Management</h2>

        <p></p>
    </div>

    <div class="panel">
        <div class="panel-body">

            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{ route('banner.create') }}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add Banner Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Position</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>


                @foreach($banners as $k => $banner)

                    <tr>
                        <td>{{ ++$k }}.</td>
                        <td>{{ $banner->title }}</td>
                        <td><img src="{{ asset($banner->thumbnail_path) }}" height="80px;" class="border" /></td>
                        <td>{{$banner->position}}</td>
                        <td>{{ $banner->status_text }}</td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('banner.edit', [$banner->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('banner.destroy',[$banner->id]) }}">
                                            {!! delete_form(route('banner.destroy', [$banner->id]))!!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
