{!! csrf_field() !!}

<div class="form-group">
    <label class="col-sm-3 control-label">Title <span class="text-danger">*</span></label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="title"
               value="{{ old('title', isset($banner->title) ? $banner->title : '') }}"/>
        <span class="validation-error">{{ $errors->first('title') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Description</label>

    <div class="col-sm-6">
        <textarea class="form-control"
                  name="description" >{{ old('description', isset($banner->description) ? $banner->description : '') }}</textarea>
        <span class="validation-error">{{ $errors->first('description') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Image</label>

    <div class="col-sm-6">
        @if(isset($banner->image))
            <img src="{{ asset($banner->thumbnail_path) }}" height="100px" alt=""/>
            <br/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Position <span class="text-danger">*</span></label>

    <div class="col-sm-6">
        <input type="text" name="position" class="form-control"
               value="{{ old('position', isset($banner->position) ? $banner->position: '')}}">
        <span class="validation-error">{{ $errors->first('position') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Button Text <span class="text-danger"></span></label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="button_text"
               value="{{ old('button_text', isset($banner->button_text) ? $banner->button_text : '') }}"/>
        <span class="validation-error">{{ $errors->first('button_text') }}</span>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Button Link <span class="text-danger"></span></label>

    <div class="col-sm-6">
        <input type="text" class="form-control" name="button_link"
               value="{{ old('button_link', isset($banner->button_link) ? $banner->button_link : '') }}"/>
        <span class="validation-error">{{ $errors->first('button_link') }}</span>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Button Target</label>
    <div class="col-sm-6">
        <input type="radio" name="button_target" value="parent" class="radio-inline" @if(empty($banner->button_target)) checked="checked" @endif
                {{ old('button_target', isset($banner->button_target) ? $banner->button_target : '')=='parent'?'checked="checked"':''}}> Available
        <input type="radio" name="button_target" value="newtab" class="radio-inline"
                {{ old('button_target', isset($banner->button_target) ? $banner->button_target : '')=='newtab'?'checked="checked"':''}}> Not Available
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Status <span class="text-danger">*</span></label>

    <div class="col-sm-6">
        <select class="form-control" name="status">
            <option value="active"
                    {{ old('status', isset($banner->status) ? $banner->status : '') == 'active' ? 'selected="selected"' : '' }}
            >Active
            </option>
            <option value="in_active"
                    {{ old('status', isset($banner->status) ? $banner->status : '') == 'in_active' ? 'selected="selected"' : '' }}
            >Inactive
            </option>
        </select>
        <span class="validation-error">{{ $errors->first('status') }}</span>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label"></label>

    <div class="col-sm-6">
        <button type="submit" class="btn btn-success btn-sm">Save</button>
        <a href="{{ route('banner.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>
