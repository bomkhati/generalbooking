@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Welcome, To Dashboard</h2>
    </div>

    <div class="row flexthis ">
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    @php
                        $bookings = count($booking);
                        $reviews = count($review);
                        $customers = count($customer);
                    @endphp
                    <div class="header">
                        {{$bookings}}
                        <h4>Customer Bookings</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        {{$reviews}}
                        <h4>Customer Reviews</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        {{$customers}}
                        <h4>Total Customer</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row flexthis ">
        <div class="col-md-12">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        <h4>Recent Bookings</h4>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-body">

                        <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Booked By</th>
                                <th>Customer Type</th>
                                <th>Booked Time & Date</th>
                                <th>Booked Service</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($booking as $k => $booking)
                                    <tr>
                                        <td>{{ ++$k }}</td>
                                        <td>{{ $booking->customers->name }}</td>
                                        <td>{{!empty($booking->customers->customer_type)?ucfirst($booking->customers->customer_type):'NA'}}</td>
                                        <td>{{ date('D F Y H:i A', strtotime($booking->time_slots)) }}</td>
                                        <td>{{ $booking->treatment }}</td>
                                        <td>
                                            <div class="booking_status_text">
                                                @if($booking->status=='canceled')
                                                    <span class="bs-badge badge-danger">  {{ $booking->status_text }}</span>
                                                @elseif($booking->status=='approved')
                                                    <span class="bs-badge badge-success"> {{ $booking->status_text }}</span>
                                                @else
                                                    <span class="bs-badge badge-warning"> {{ $booking->status_text }}</span>
                                                @endif
                                            </div>
                                            <div class="booking_status_form" style="display: none;">
                                                <select class="form-control status_form" onchange="updateForm(this, {{$booking->id}})">
                                                    <option value="canceled" {{$booking->status=='canceled' ? 'selected="selected"' :''}}>Cancelled</option>
                                                    <option value="approved" {{$booking->status=='approved' ? 'selected="selected"' :''}}>Approved</option>
                                                    <option value="pending" {{$booking->status=='pending' ? 'selected="selected"' :''}}>Pending</option>
                                                </select>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="dropdown">
                                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                                    <i class="glyph-icon icon-cog"></i>
                                                    <i class="glyph-icon icon-chevron-down"></i>
                                                </a>
                                                <ul class="dropdown-menu float-right">
                                                    <li>
                                                        <a href="{{ route('customer-booking.destroy',[$booking->id]) }}">
                                                            {!! delete_form(route('customer-booking.destroy',[$booking->id]))!!}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>
        function  updateForm(form, bookingId) {
            status = $(form).val();

            if(status=='approved'){
                html =  '<span class="bs-badge badge-success">  Approved </span>';
            }
            else if(status=='canceled'){
                html =  '<span class="bs-badge badge-danger">  Cancelled </span>';
            }
            else{
                html =  '<span class="bs-badge badge-primary">  Pending </span>';
            }

            token = "{{ csrf_token() }}";
            $.ajax
            ({
                type: "get",
                url: '{{route('superadmin.ajax.booking.status')}}',

                data: "status="+status+"&bookingId="+bookingId+"&_token="+token,
                success: function(result)
                {
                    $(form).parent('.booking_status_form').hide();
                    $(form).parent('.booking_status_form').prev('.booking_status_text').html(html);
                }
            });
        }
        $(document).ready(function () {
            $('.booking_status_text').click(function () {
                var html = $.trim($(this).find('span').html());
                var compare = "Approved";
                if(html.toLowerCase() != compare.toLowerCase()) {
                    $(this).next('.booking_status_form').toggle();
                }
            })


        })
    </script>
@endsection
