{!! csrf_field() !!}
<div class="clearfix"></div>

<div class="form-group">
    <label class="col-sm-3 control-label">Image(*)</label>
    <div class="col-sm-6">
        @if(isset($gallery->file))
            <img src="{{ asset($gallery->thumbnail_path) }}" height="100px" alt=""/>
            <br/>
            <br/>
        @endif
        <input type="file" class="form-control" name="file"/>
        <span class="validation-error">{{ $errors->first('file') }}</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Alternate Text</label>
    <div class="col-sm-6">
        <input type="text" name="title" id="title" value="{{ old('title', isset($gallery->title) ? $gallery->title : '') }}" class="form-control">
        <i class="errormsg">{{ $errors->first('title') }}</i>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-6">
        <input type="submit" name="" value="Save" class="btn btn-success">
        <a href="{{ route('gallery.index') }}" class="btn btn-default btn-sm">
            <i class="icon-reply"></i> Back
        </a>
    </div>
</div>
