@extends('layouts.superadmin')
@section('content')
    <div id="page-title">
        <h2>Gallery</h2>

    </div>

    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{route('gallery.create')}}">
                    <i class="fa fa-aw fc-agenda-axis"></i> Add Gallery Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($galleries as $k => $gallery)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{$gallery->title }}</td>
                        <td><img src="{{asset($gallery->image_path)}}" height="80px;" class="border"></td>
                        <td>
                            <div class="dropdown">
                                <a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                                    <i class="glyph-icon icon-cog"></i>
                                    <i class="glyph-icon icon-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu float-right">
                                    <li>
                                        <a href="{{ route('gallery.edit', [$gallery->id]) }}" title="">
                                            <i class="glyph-icon icon-pencil mrg5R"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('gallery.destroy',[$gallery->id]) }}" title="">
                                            {!! delete_form(route('gallery.destroy', [$gallery->id]))!!}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-right">
                {!! $galleries->render() !!}
            </div>
        </div>
    </div>

@stop

