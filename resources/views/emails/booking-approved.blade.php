@include('layouts.emails.header')

<tr>
    <td valign="top">
        <table border="0" cellpadding="0" cellspacing="0" class=
        "brdBottomPadd-two" id="templateContainer" width="100%">
            <tr>
                <td class="bodyContent" valign="top">
                    <h1>Hello {{ \Auth::user()->name }},</h1>
                </td>
            </tr>
            <tr align="top">
                <td class="bodyContentImage" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <p>Congratulations !!!. You booking has been <b>confirmed</b>.</p>
                                <p>Your booking date and time is <b>{{ date('D F Y H:i A', strtotime($booking->time_slots)) }}</b></p>
                                <p><b>Booking Address:</b><a href="https://www.google.com/maps/dir//-37.8181373,144.9966978/@-37.818137,144.996698,15z?hl=en-US"> 209 Bridge Road Richmond, Victoria 3121</a> </p>
                                <p><b>Contact Number:</b> (03) 9939 4498</p>
                                <p><small>(Note: If you want ot cancel the booking. Please give us the call <a href="tel:(03) 9939 4498">(03) 9939 4498</a> prior 4 hours of the appointment time.)</small></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

@include('layouts.emails.footer')
