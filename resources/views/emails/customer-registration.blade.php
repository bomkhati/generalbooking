
@include('layouts.emails.header')

<tr>
    <td align="center" valign="top">
        <table border="0" cellpadding="10" cellspacing="0" class=
        "brdBottomPadd-two" id="templateContainer" width="100%">
            <tr>
                <td class="bodyContent" valign="top">
                    <h1>Hello!</h1>
                    <p>You're almost ready to get started with La’ Nails Studio.
                        Please confirm your email, so we know it's really you.</p>
                </td>
            </tr>

            <tr align="top">
                <td class="bodyContent" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0"  width="100%">
                        <tr>
                            <td>
                                <a href="{{ $notifiable }}" class="button">Confirm Email</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="top">
                <td class="bodyContent" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0"  width="100%">
                        <tr>
                            <td>
                                <small>If you’re having trouble clicking the "Confirm Email" button, copy and paste the URL below into your web browser:<br/>
                                    {{ $notifiable }}
                                </small>


                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
    </td>
</tr>

@include('layouts.emails.footer')
