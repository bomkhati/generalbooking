@include('layouts.emails.header')

<tr>
    <td valign="top">
        <table border="0" cellpadding="0" cellspacing="0" class=
        "brdBottomPadd-two" id="templateContainer" width="100%">
            <tr>
                <td class="bodyContent" valign="top">
                    <h1>Hello {{ \Auth::user()->name }},</h1>
                    <p>We have successfully received your review.</p>
                </td>
            </tr>
            <tr align="top">
                <td class="bodyContentImage" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <p>Thank you so much for your review.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

@include('layouts.emails.footer')
