@include('layouts.frontweb.head')
@include('layouts.frontweb.header')

<div id="page-wrapper">
    @include('layouts.frontweb.sidebar')
    <div id="page-content-wrapper">
        <div id="page-content">

            <div class="container">
                @include('layouts.frontweb.flash-message')
                @yield('content')
            </div>
        </div>
    </div>
</div>
@yield('page-specific-scripts')
@include('layouts.frontweb.footer')