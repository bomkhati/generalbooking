<!-- Footer -->
@if($footerSettings->count() >0)
    <footer class="site-footer footer-overlay bg-img-fix" style="background-image: url(images/background/bg1.jpg); background-position: center bottom; background-size: cover;">
        <!-- footer bottom part -->
        @foreach($footerSettings as $footerSetting)
            <div class="footer-bottom text-center">
                <div class="container p-tb10">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 m-b30">
                            <img src="{{asset('storage/frontweb/assets/logo.png')}}" alt="" width="100">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="widget-link ">
                                <span>Copyright © {{ date('Y') }} {!! $footerSetting->copyright !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 m-t20">
                            <img src="{{asset('storage/frontweb/assets/footerLogo.jpg')}}" width="100">
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </footer>
@endif
<!-- Footer END-->
<!-- scroll top button -->
<button class="scroltop fa fa-chevron-up" ></button>
</div>

<!-- custom fuctions  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" type="text/css" />

<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css" rel="stylesheet">
<script>
    $(document).ready(function () {
        var route = '';
        $('#datepicker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '+1 d',
            todayHighlight: true,
            toggleActive: true
        });
        route = "{{route('general-booking-guest')}}"
        @if(\Auth::guard('customer')->check())
            route = "{{route('available.slots')}}";
        @endif

        $('#datepicker').on('changeDate', function() {
            $('#my_hidden_input').val(
                $('#datepicker').datepicker('getFormattedDate')
            );
            var date = $('#my_hidden_input').val();
            $.ajax
            ({
                type: "get",
                url: route,
                data: "date="+date,
                success: function(result)
                {
                    //do something
                    if(result==0){
                        $('#time-slots').html('No Booking Available for the day');
                    }
                    else {
                        $('#time-slots').html(result);
                    }
                }
            });
        });
        //Radio button active JS
        $('input').click(function () {
            $('input:not(:checked)').parent().parent().removeClass("time_slot_active");
            $('input:checked').parent().parent().addClass("time_slot_active");
        });
        $('input:checked').parent().parent().addClass("time_slot_active");

        //Radio button switch value
        $( "input" ).on( "click", function() {
            var time = $('input[name="booking_time"]:checked').val();
            console.log(time);
            console.log( $('#my_hidden_input').val());
        });
    })
</script>
<script>
    jQuery(document).ready(function() {
        dz_rev_slider_1();

    });	/*ready*/

    $('.single-item').slick({

        autoplay: true, speed: 500,
    });

</script>

</body>
</html>
