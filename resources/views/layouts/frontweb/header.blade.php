<div id="page-header" class="bg-black">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="{{route('customer.dashboard')}}" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
        <a href="{{route('customer.dashboard')}}" class="logo-content-big" title="MonarchUI">
            AMEB <i>Portal</i>
            <span>The perfect solution for user interfaces</span>
        </a>
        <a href="{{route('customer.dashboard')}}" class="logo-content-small" title="MonarchUI">
            AMEB <i>Portal</i>
            <span>The perfect solution for user interfaces</span>
        </a>
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>
    </div>
</div>