@include('layouts.superadmin.head')
@include('layouts.superadmin.header')

<div id="page-wrapper">
    @include('layouts.superadmin.sidebar')
    <div id="page-content-wrapper">
        <div id="page-content">

            <div class="container">
                @include('layouts.superadmin.flash-message')
                @yield('content')
            </div>
        </div>
    </div>
</div>
@yield('page-specific-scripts')
@include('layouts.superadmin.footer')