<!-- WIDGETS -->
<script type="text/javascript" src="{{ asset('resources/superadmin/bootstrap/js/bootstrap.js') }}"></script>

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/progressbar/progressbar.js') }}"></script>


<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/input-switch/inputswitch-alt.js') }}"></script>


<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/slimscroll/slimscroll.js') }}"></script>


<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/slidebars/slidebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/slidebars/slidebars-demo.js') }}"></script>


<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/charts/piegage/piegage.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/charts/piegage/piegage-demo.js') }}"></script>

<!-- Screenfull -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/screenfull/screenfull.js') }}"></script>

<!-- Content box -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/content-box/contentbox.js') }}"></script>

<!-- Overlay -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/overlay/overlay.js') }}"></script>

<!-- Widgets init for demo -->

<script type="text/javascript" src="{{ asset('resources/superadmin/js-init/widgets-init.js') }}"></script>

<!-- Theme layout -->


<!-- Theme switcher -->

<!-- jQueryUI Spinner -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/spinner/spinner.js') }}"></script>
<script type="text/javascript">
    /* jQuery UI Spinner */

    $(function () {
        "use strict";
        $(".spinner-input").spinner();
    });
</script>

<!-- jQueryUI Autocomplete -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/autocomplete/autocomplete.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/autocomplete/menu.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/autocomplete/autocomplete-demo.js') }}"></script>

<!-- Touchspin -->

<!--<link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/touchspin/touchspin.css') }}">-->
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/touchspin/touchspin.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/touchspin/touchspin-demo.js') }}"></script>

<!-- Input switch -->

{{--<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>--}}


<!--<link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/input-switch/inputswitch.css') }}">-->
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/input-switch/inputswitch.js') }}"></script>
<script type="text/javascript">
    /* Input switch */

    $(function () {
        "use strict";
        $('.input-switch').bootstrapSwitch();
    });
</script>

<!-- Textarea -->

<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/textarea/textarea.js') }}"></script>
<script type="text/javascript">
    /* Textarea autoresize */

    $(function () {
        "use strict";
        $('.textarea-autosize').autosize();
    });
</script>

<!-- Multi select -->

<!--<link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/multi-select/multiselect.css') }}">-->
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/multi-select/multiselect.js') }}"></script>
<script type="text/javascript">
    /* Multiselect inputs */

    $(function () {
        "use strict";
        $(".multi-select").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });
</script>

<!-- Uniform -->

<!--<link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/uniform/uniform.css') }}">-->
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/uniform/uniform.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/uniform/uniform-demo.js') }}"></script>

<!-- Chosen -->


<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/chosen/chosen.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/chosen/chosen-demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/datatable/datatable.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/datatable/datatable-bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/datatable/datatable-fixedcolumns.js') }}"></script>


<!-- Widgets init for demo -->
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/datepicker/datepicker.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('resources/superadmin/js/custom.js') }}"></script>--}}

<script>
    siteUrl = "<?php echo url('/') ?>";

</script>
<script type="text/javascript" src="{{ asset('resources/superadmin/plugins/delete-confirmation/bootstrap-confirm-delete.js') }}"></script>
<script>

    $(function () {
        $('[data-toggle=confirmation]').bootstrap_confirm_delete({
            callback: function (data) {
                console.log(data.data.originalObject);
                data.data.originalObject.closest('form').submit();
            }
        });

    })

</script>
<script type="text/javascript" src="{{ asset('resources/superadmin/widgets/ckeditor/ckeditor.js') }}"></script>
<script>

    // This code is generally not necessary, but it is here to demonstrate
    // how to customize specific editor instances on the fly. This fits well
    // this demo because we have editable elements (like headers) that
    // require less features.

    // The "instanceCreated" event is fired for every editor instance created.
    CKEDITOR.on( 'instanceCreated', function( event ) {
        var editor = event.editor,
            element = editor.element;

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }
    });

</script>
</div>
</body>
</html>
