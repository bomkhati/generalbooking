<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title> Booking System </title>
    <link rel="icon" href="{{ asset('resources/frontweb/favicon.png') }}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('resources/frontweb/favicon.png') }}" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="_token" content="{!! csrf_token() !!}" />

    <!-- Favicons -->

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('resources/superadmin/images/icons/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('resources/superadmin/images/icons/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('resources/superadmin/images/icons/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('resources/superadmin/images/icons/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('resources/superadmin/image-resources/ameb_logo_small.png')}}">



    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/bootstrap/css/bootstrap.css') }}">


    <!-- HELPERS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/backgrounds.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/boilerplate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/border-radius.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/grid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/page-transitions.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/spacing.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/typography.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/utils.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/colors.css') }}">

    <!-- ELEMENTS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/badges.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/buttons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/content-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/dashboard-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/forms.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/images.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/info-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/invoice.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/loading-indicators.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/menus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/panel-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/response-messages.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/responsive-tables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/ribbon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/social-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/tables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/tile-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/elements/timeline.css') }}">

    <!-- ICONS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/icons/fontawesome/fontawesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/icons/linecons/linecons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/icons/spinnericon/spinnericon.css') }}">


    <!-- WIDGETS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/accordion-ui/accordion.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/calendar/calendar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/carousel/carousel.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/charts/justgage/justgage.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/charts/morris/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/charts/piegage/piegage.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/charts/xcharts/xcharts.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/chosen/chosen.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/colorpicker/colorpicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/datatable/datatable.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/datepicker/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/datepicker-ui/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/dialog/dialog.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/dropdown/dropdown.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/dropzone/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/file-input/fileinput.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/input-switch/inputswitch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/input-switch/inputswitch-alt.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/ionrangeslider/ionrangeslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/jcrop/jcrop.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/jgrowl-notifications/jgrowl.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/loading-bar/loadingbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/maps/vector-maps/vectormaps.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/markdown/markdown.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/modal/modal.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/multi-select/multiselect.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/multi-upload/fileupload.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/nestable/nestable.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/noty-notifications/noty.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/popover/popover.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/pretty-photo/prettyphoto.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/progressbar/progressbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/range-slider/rangeslider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/slidebars/slidebars.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/slider-ui/slider.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/summernote-wysiwyg/summernote-wysiwyg.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/tabs-ui/tabs.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/theme-switcher/themeswitcher.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/timepicker/timepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/tocify/tocify.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/tooltip/tooltip.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/touchspin/touchspin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/uniform/uniform.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/wizard/wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/widgets/xeditable/xeditable.css') }}">

    <!-- SNIPPETS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/chat.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/files-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/login-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/notification-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/progress-box.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/todo.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/user-profile.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/snippets/mobile-navigation.css') }}">

    <!-- APPLICATIONS -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/applications/mailbox.css') }}">

    <!-- Admin theme -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/themes/admin/layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/themes/admin/color-schemes/default.css') }}">



    <!-- Components theme -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/themes/components/default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/themes/components/border-radius.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">--}}

    <!-- Admin responsive -->

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/responsive-elements.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/helpers/admin-responsive.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('resources/superadmin/plugins/delete-confirmation/bootstrap-confirm-delete.css') }}"/>

    <!-- JS Core -->

    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-ui-core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-ui-widget.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-ui-mouse.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-ui-position.js') }}"></script>
    <!--<script type="text/javascript" src="{{ asset('resources/superadmin/js-core/transition.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/superadmin/js-core/jquery-cookie.js') }}"></script>


    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>




    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>
    <style>
        .validation-error
        {
            color: red;
        }
    </style>



</head>


<body>
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
