<div id="page-sidebar" style="background-color: rgb(255, 255, 255); height: 885px; border-right: 1px solid #ede9e9;">
    <div class="scroll-sidebar">
        @php
            $menuFlag=0;
        @endphp
        <ul id="sidebar-menu">
            <li class="header"></li>
            <li class="header"><span>Manage</span></li>
            <li class="header"></li>
                <li>
                    <a href="{{URL::route('superadmin.dashboard')}}" title="Admin Dashboard" class="{{ request()->is('administrator/dashboard*') ? 'collapsible active' : '' }}">
                        <i class="glyph-icon icon-linecons-tv"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="#" title="User" class="collapsible">
                        <i class="glyph-icon icon-linecons-user"></i>
                        <span>Customers</span>
                    </a>

                    <div class="content sidebar-submenu">

                        <ul>
                            <li><a href="{{route('user.index')}}"
                                   title="Manage Banner"><span>Customers</span></a></li>
                        </ul>

                    </div>
                    <!-- .sidebar-submenu -->
                </li>

                <li>
                    <a href="#" title="FAQ" class="collapsible">
                        <i class="glyph-icon icon-info"></i>
                        <span>FAQ</span>
                    </a>

                    <div class="content sidebar-submenu">
                        <ul>
                            <li><a href="{{route('faq.index')}}" title="Manage FAQ"><span>Manage FAQ</span></a></li>
                            <li><a href="{{route('faq-category.index')}}" title="FAQ Category"><span>FAQ Category</span></a></li>
                        </ul>
                    </div>
                    <!-- .sidebar-submenu -->
                </li>
            <li class="header"></li>
            <li class="header"><span>Booking</span></li>
            <li class="header"></li>
            <li>
                <a href="#" title="Site Settings" class="collapsible">
                    <i class="glyph-icon icon-ticket"></i>
                    <span>Booking Setting</span>
                </a>

                <div class="content sidebar-submenu">

                    <ul>
                        <li><a href="{{ route('category.index') }}" title="Booking Category"><span>Booking Category</span></a></li>
                        <li><a href="{{ route('subcategory.index') }}" title="Booking Sub-Category"><span>Booking Sub-Category</span></a></li>
                        <li><a href="{{ route('default-setting.index') }}" title="Default Setting"><span>Default Setting</span></a></li>
                        <li><a href="{{ route('general-setting.index') }}" title="Special Setting"><span>Special Setting</span></a></li>
                    </ul>

                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li>
                <a href="#" title="Site Settings" class="collapsible">
                    <i class="glyph-icon icon-calendar"></i>
                    <span>Recent Booking</span>
                </a>

                <div class="content sidebar-submenu">

                    <ul>
                        <li><a href="{{ route('customer-booking.index') }}" title="Alternate dashboard"><span>Total Bookings</span></a></li>
                    </ul>

                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li>
                <a href="#" title="Site Settings" class="collapsible">
                    <i class="glyph-icon icon-file"></i>
                    <span>Booking Reports</span>
                </a>

                <div class="content sidebar-submenu">

                    <ul>
                        <li><a href="{{ route('reports') }}" title="Alternate dashboard"><span>Booking Reports</span></a></li>
                    </ul>

                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li>
                <a href="#" title="Site Settings" class="collapsible">
                    <i class="glyph-icon icon-pencil"></i>
                    <span>Reviews</span>
                </a>

                <div class="content sidebar-submenu">

                    <ul>
                        <li><a href="{{ route('review.index') }}" title="Alternate dashboard"><span>Customer Reviews</span></a></li>
                    </ul>

                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li class="header"></li>
            <li class="header"><span>Website Settings</span></li>
            <li class="header"></li>
            <li>
                <a href="#" title="Site Settings" class="collapsible">
                    <i class="glyph-icon icon-linecons-params"></i>
                    <span>Site Setting</span>
                </a>

                <div class="content sidebar-submenu">

                    <ul>
                        <li><a href="{{ route('option.index') }}" title="Alternate dashboard"><span>General</span></a></li>
                        <li><a href="{{route('tracking-code.index')}}" title="Manage Footer Setting"><span>Tracking Codes </span></a></li>
                        <li><a href="{{route('site-maintenance.index')}}" title="Maintenance Mode"><span>Maintenance Mode</span></a></li>
                        <li><a href="{{route('site-notice.index')}}" title="Manage Notice"><span>Manage Notice</span></a></li>
                        <li><a href="{{route('footer-setting.index')}}" title="Manage Footer Setting"><span>Footer Settings</span></a></li>
                    </ul>

                </div>
                <!-- .sidebar-submenu -->
            </li>
            <li>
                <a href="{{ route('admin-setting.edit',['id'=>\Illuminate\Support\Facades\Auth::user()->id]) }}">
                    <i class="glyph-icon icon-edit"></i>
                    <span>Edit profile</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin-setting.change-password',['id'=>\Illuminate\Support\Facades\Auth::user()->id]) }}">
                    <i class="glyph-icon icon-key"></i>
                    <span>Change Password</span>
                </a>
            </li>
            <li>
                <a href="{{ route('superadmin.logout') }}">
                    <i class="glyph-icon icon-lock"></i>
                    <span>Logout</span>
                </a>
            </li>

        </ul>
        <!-- #sidebar-menu -->


    </div>
</div>
<style>
    #sidebar-menu li {
        border-bottom: 1px dotted #d7d7d5;
    }
    #sidebar-menu > li > a, #sidebar-menu > li > a:focus {
        color: #514f4f;
    }
    .active #sidebar-menu > li > a:hover {
        color: #ffffff !important;
    }
    .collapsible {

        width: 100%;

    }
    .collapsible i, .collapsible a:hover, .active a:hover, .active a:focus, .active a:active {
        color: #ffffff ;
    }

    #sidebar-menu  .active, .collapsible:hover, .collapsible:hover  {
        background-color: #414040;
        color: #fff !important;

    }

    .collapsible:after {
        content: '\002B';
        color: #898989;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    #sidebar-menu .active:after {
        content: "\2212";
        color: #6060da;
    }

    .content {
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    #sidebar-menu > li > a {
        padding: 0 6px 0 0;
        border-radius: 3px;
        font-size: 13px;
        border: transparent solid 1px;
        line-height: 32px;
        height: 34px;
    }
    #sidebar-menu .active, .collapsible:hover, .collapsible:hover {
        background-color: #f8f8f8;
        color: #000000 !important;
    }
    #sidebar-menu li .sidebar-submenu {
        margin: 0px 0;
    }
    .glyph-icon, .active > .glyph-icon {
        color: #76767b !important;
    }
    #sidebar-menu li .sidebar-submenu ul li a, #sidebar-menu li .sidebar-submenu ul li a:hover, #sidebar-menu li .sidebar-submenu ul li a.sfActive {
        background: #ffffff;
        color: #686868;
        padding-left: 25px;
    }
    #sidebar-menu li .sidebar-submenu ul li {
        border-color: #efeded;
    }
    #sidebar-menu > li > a, #sidebar-menu > li > a:focus {
        color: #76767b;
    }
    .bg-black {
        background: #f3f3f3;
        border-bottom: 1px solid #dfdfdf;
    }

</style>
<script>
    function menuInit() {
        var coll = document.getElementsByClassName("collapsible");
        var i;
        for (i = 0; i < coll.length; i++) {
            var activeClass = document.getElementById("activeMenu");
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");

                var content = this.nextElementSibling;
                $(this).next('div').toggle();
                if (content.style.maxHeight){
                    content.style.maxHeight = null;
                } else {
                    content.style.maxHeight = content.scrollHeight + "px";
                }
            });
        }
    }
    menuInit();
    var mainPage = '{{$menuFlag}}';
    $('document').ready(function () {
        if(mainPage==1){
            $('.sfActive').attr('id', 'activeMenu');
        }
        else {
            $('.sfActive').parents('.content').prev('a').attr('id', 'activeMenu')
        }
        $('#activeMenu').addClass('active');

        $('#activeMenu').next('div').toggle();

        $('#activeMenu').next('div').css('max-height', '100%');
        var pageHeight = $(document).height();
        $('#page-sidebar').css('height', pageHeight+'px');

    });

</script>
