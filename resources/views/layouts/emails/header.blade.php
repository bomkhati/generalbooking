<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title>La’ Nails Studio - Booking</title>

    <style type="text/css" id="media-query">
        body {
            margin: 0;
            padding: 0;
            font-family: Avenir,Helvetica,sans-serif;
            box-sizing: border-box;
            color: #000000;
            font-size: 14px;
            line-height: 1.5em;
            margin-top: 0;
            text-align: left;

        }
        body p {
            font-family: Avenir,Helvetica,sans-serif;
            box-sizing: border-box;
            color: #000000;
            font-size: 14px;
            line-height: 1.5em;
            margin-top: 0;
            text-align: left;
            word-break: break-word;
        }
        h1 {
            font-size: 18px;
            color: #000000;
        }

        .button {
            background: #D7D7C1;
            display: block;
            margin: 0 auto;
            margin-top: 10px;
            text-decoration: none;
            text-align: center;
            margin-bottom: 10px;
            font-family: Avenir,Helvetica,sans-serif;
            box-sizing: border-box;
            border-radius: 3px;
            color: #000000;
            display: inline-block;
            text-decoration: none;
            border-top: 10px solid #D7D7C1;
            border-right: 18px solid #D7D7C1;
            border-bottom: 10px solid #D7D7C1;
            border-left: 18px solid #D7D7C1;
        }

        .button:hover {
            /*background: #EAEADF;*/
        }

        table, tr, td {
            vertical-align: top;
            border-collapse: collapse;
            word-break: break-word;
            color: #000000;
            font-size: 14px;
        }
        tr {
            padding-top:20px;
            margin-bottom:20px;
        }

        .ie-browser table, .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        [owa] .img-container div, [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
            width: 500px !important;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
            width: 164px !important;
        }

        .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
            width: 328px !important;
        }

        .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
            width: 250px !important;
        }

        .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
            width: 166px !important;
        }

        .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
            width: 125px !important;
        }

        .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
            width: 83px !important;
        }

        .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
            width: 71px !important;
        }

        .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
            width: 62px !important;
        }

        .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
            width: 55px !important;
        }

        .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
            width: 50px !important;
        }

        .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
            width: 45px !important;
        }

        .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
            width: 41px !important;
        }

        @media only screen and (min-width: 520px) {
            .block-grid {
                width: 500px !important;
            }

            .block-grid .col {
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 500px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 164px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 328px !important;
            }

            .block-grid.two-up .col {
                width: 250px !important;
            }

            .block-grid.three-up .col {
                width: 166px !important;
            }

            .block-grid.four-up .col {
                width: 125px !important;
            }

            .block-grid.five-up .col {
                width: 100px !important;
            }

            .block-grid.six-up .col {
                width: 83px !important;
            }

            .block-grid.seven-up .col {
                width: 71px !important;
            }

            .block-grid.eight-up .col {
                width: 62px !important;
            }

            .block-grid.nine-up .col {
                width: 55px !important;
            }

            .block-grid.ten-up .col {
                width: 50px !important;
            }

            .block-grid.eleven-up .col {
                width: 45px !important;
            }

            .block-grid.twelve-up .col {
                width: 41px !important;
            }
        }

        @media (max-width: 520px) {
            .block-grid, .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: calc(100% - 40px) !important;
            }

            .col {
                width: 100% !important;
            }

            .col > div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
            }
        }

    </style>

    <style type="text/css" id="media-query-bodytag">
        @media (max-width: 520px) {
            .block-grid {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col > div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
            }
        }
    </style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<!--[if IE]>
<div class="ie-browser"><![endif]-->
<!--[if mso]>
<div class="mso-container"><![endif]-->
<div style="width: 100%">
    <table class="nl-container"
           style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px; width:600px; margin: 0 auto;background-color: #FFFFFF;">
        <tbody>
        <tr style="vertical-align: top">
            <td style="word-break: break-word; border-collapse: collapse !important;vertical-align: top">
                <!--[if (mso)|(IE)]>
                <table width="520px" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td align="center" style="background-color: #FFFFFF;"><![endif]-->


                <div style="background-color:transparent;">
                    <div style="Margin: 0 auto;min-width: 320px;max-width: 520px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                         class="block-grid ">
                        <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="background-color:transparent;" align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" style="width: 500px;">
                                            <tr class="layout-full-width" style="background-color:transparent;">
                            <![endif]-->

                            <!--[if (mso)|(IE)]>
                            <td align="center" width="520px"
                                style=" width:500px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                                valign="top"><![endif]-->
                            <div class="col num12"
                                 style="min-width: 320px;max-width: 500px;display: table-cell;vertical-align: top;">
                                <div style="background-color: transparent; width: 100% !important;">
                                    <!--[if (!mso)&(!IE)]><!-->
                                    <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                        <!--<![endif]-->


                                        <div align="center" class="img-container center fullwidth"
                                             style="padding-right: 0px;  padding-left: 0px;">
                                            <!--[if mso]>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding-right: 0px; padding-left: 0px;" align="center">
                                            <![endif]--><br/><br/>
                                            <a href="http://www.lanailsstudio.com.au/" target="_blank">
                                                <img class="center fullwidth" align="center" border="0"
                                                     src="http://www.lanailsstudio.com.au/storage/frontweb/assets/logo.png"
                                                     alt="La' Nails Studio" title="http://www.lanailsstudio.com.au/ logo"
                                                     style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 368px"
                                                     width="205px">
                                            </a><br/><br/>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                        </div>


                                        <!--[if (!mso)&(!IE)]><!--></div>
                                    <!--<![endif]-->
                                </div>
                            </div>
                            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>

