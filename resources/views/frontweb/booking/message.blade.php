@extends('layouts.customer')
@section('content')
    <div class="section-full bg-white contact-style-1 bgeffect">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="dez-tabs vertical">
                        <ul class="nav nav-tabs">
                            <li>
                                <a href="{{route('customer.dashboard')}}">
                                    <i class="fa fa-dashboard fa-lg"></i> Dashboard
                                </a>
                            </li>
                            <li>
                                <a href="{{route('booking')}}">
                                    <i class="fa fa-calendar fa-lg"></i> Request Booking
                                </a>
                            </li>
                            <li>
                                <a href="{{route('my-booking')}}">
                                    <i class="fa fa-user fa-lg"></i> My Bookings
                                </a>
                            </li>
                            <li>
                                <a href="{{route('my-review.index')}}">
                                    <i class="fa fa-pencil fa-lg"></i> Provide Review
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <i class="fa fa-lock fa-lg"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="alert section-padding-calender">
                        @if($msg=='success')
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> Your booking has been made.
                            </div>
                        @else
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error!</strong> Your booking could not be confirmed at the moment. Please try again later.
                            </div>

                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@endsection
