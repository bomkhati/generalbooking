@extends('layouts.customer')
@section('content')
    <div id="page-title">
        <h3>Please select the date and time and proceed for bookings.</h3>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" id="cmsPageForm" method="POST" action="{{ route('booking.request') }}"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="col-md-3 calenderbooking">
                        <div id="datepicker"></div>
                        <input type="hidden" id="my_hidden_input" name="booking_date">
                    </div>

                    <div class="col-md-9 text-center card" id="booking-slots">
                        <div class="card-body">
                            <div class="col-md-12">
                                <ul class="time-slots" id="time-slots">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select Services</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="treatment">
                                <option value="n/a" id="select-1545373446907-0">Select treatment</option>
                                <option value="Mani Bar -> Classic Manicure">Classic Manicure</option>
                                <option value="Mani Bar -> Deluxe Manicure">Deluxe Manicure</option>
                                <option value="Hand Gel Polish (Shellac) -> Classic Shellac mani">Classic Shellac mani</option>
                                <option value="Hand Gel Polish (Shellac) -> Deluxe Shellac mani">Deluxe Shellac mani</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Full set Acrylic / Gel">Full set Acrylic / Gel</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Full set acrylic / Gel Overlay">Full set acrylic / Gel Overlay</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Refill">Refill</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Rebalance" >Rebalance</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Acrylic/ Gel nail removal">Acrylic/ Gel nail removal</option>
                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Single nail repair">Single nail repair</option>
                                <option value="Pedi Bar -> Classic Spa Pedicure" >Classic Spa Pedicure</option>
                                <option value="Pedi Bar -> Deluxe Spa Pedicure" >Deluxe Spa Pedicure</option>
                                <option value="Foot Gel Polish (Shellac) -> Classic Shellac" >Classic Shellac</option>
                                <option value="Foot Gel Polish (Shellac) -> Deluxe Shellac Treatment">Deluxe Shellac Treatment</option>
                                <option value="Foot Gel Polish (Shellac) -> Shellac Removal" >Shellac Removal</option>
                                <option value="Foot Gel Polish (Shellac) -> Polish change">Polish change</option>
                                <option value="Foot Gel Polish (Shellac) -> Hand or foot Paraffin Wax treatment">Hand or foot Paraffin Wax treatment</option>
                                <option value="Nail Design (hand and foot) -> *Nail art / nail">*Nail art / nail</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Extra Comments</label>
                        <div class="col-sm-9">
                            <textarea class="ckeditor" cols="80" id="editor1" name="comment" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <input type="submit" name="" value="Save" class="btn btn-success">
                            <a href="{{ route('customer.dashboard') }}" class="btn btn-default btn-sm">
                                <i class="icon-reply"></i> Back
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <style>
        .time-slots .disabled {
            background: gray;
        }
        .custom-control-input:disabled ~ .custom-control-description{
            color: #ffffff;
        }
        .custom-control-input:disabled ~ .custom-control-space{
            color: #ffffff;
        }
    </style>

@stop

