@extends('layouts.customer')
@section('content')
    <div class="section-full bg-white contact-style-1 bgeffect">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert section-padding-calender">
                        @if($msg=='success')
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> Your booking has been made.
                            </div>
                        @else
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error!</strong> Your booking could not be confirmed at the moment. Please try again later.
                            </div>

                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
@endsection
