@extends('layouts.customer')
@section('content')
    <div id="page-title">
        <h3>Please fill the form for review</h3>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="panel-heading text-right">
            </div>
            <div class="example-box-wrapper">
                <form class="form-horizontal bordered-row" id="cmsPageForm" method="POST" action="{{ route('my-review.store') }}"
                      enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Review</label>
                        <div class="col-sm-9">
                         <textarea class="ckeditor" cols="80" id="editor1" name="review" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-6">
                            <input type="submit" name="" value="Save" class="btn btn-success">
                            <a href="{{ route('customer.dashboard') }}" class="btn btn-default btn-sm">
                                <i class="icon-reply"></i> Back
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@stop

