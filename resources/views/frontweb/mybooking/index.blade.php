@extends('layouts.customer')
@section('content')

    <div id="page-title">
        <h2>My Recent Bookings</h2>

        <p></p>
    </div>

    <div class="panel">
        <div class="panel-body">

            <div class="panel-heading text-right">
                <a class="btn btn-sm btn-success add-button" href="{{route('booking')}}">
                    <i class="fa fa-aw fc-agenda-axis"></i>Request Booking Here
                </a>
            </div>

            <table id="datatable-fixedcolumns" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Booked Time & Date</th>
                    <th>Booked Service</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>


                @if($totalBookings->count() > 0)
                    @foreach($totalBookings as $k => $totalBooking)
                    <tr>
                        <td>{{ ++$k }}.</td>
                        <td>{{ date('D F Y H:i A', strtotime($totalBooking->time_slots)) }}</td>
                        <td>{{ $totalBooking->treatment }}</td>
                        <td>
                            <div class="booking_status_text">
                                @if($totalBooking->status=='canceled' || $totalBooking->status=='canceled')
                                    <span class="bs-badge badge-danger">  {{ $totalBooking->status_text }}</span>
                                @elseif($totalBooking->status=='approved')
                                    <span class="bs-badge badge-success"> {{ $totalBooking->status_text }}</span>
                                @else
                                    <span class="bs-badge badge-warning"> {{ $totalBooking->status_text }}</span>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="4">No Bookings found.</td>
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>
@stop
