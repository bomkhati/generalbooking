@extends('layouts.frontweb')
@section('title', 'Customer Reviews')
@section('content')
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr tb overlay-white-light " style="background-image:url({{asset('storage/frontweb/assets/images/banner.jpg')}});">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h2 class="text-red">Customer Reviews</h2>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <ul class="list-inline">
                            <li><a href="/">Home</a></li>
                            <li>Customer Reviews</li>
                        </ul>
                    </div>
                    <!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
        <div class="content-block">
            <!-- About Us -->
        @if($reviews->count() >0)
           <div class="section-full bg-white content-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="sort-title clearfix text-center">
                                    <h4><i class="fa fa-leaf" style="color:red"></i> Customer Reviews <i class="fa fa-leaf" style="color:red"></i></h4>
                                </div><!-- Defult Accordion END -->
                                <div class="dez-accordion box-sort-in m-b30 red" id="accordion1">

                                @foreach($reviews as $k => $review)
                                        @php
                                            $user = $review->user_id;
                                            $name = $review->full_name;
                                        @endphp
                                        <div class="panel">
                                            <div class="acod-head">
                                                @if($user != null)
                                                        <h5 class="acod-title">
                                                            <a data-toggle="collapse" href="#{{$k}}" @if($k != 1){ class="collapsed" } @endif data-parent="#{{$k}}">
                                                                <i class="fa fa-pencil"></i> {{ $review->customers->name }} </a>
                                                        </h5>
                                                @endif
                                                @if($name != null)
                                                    <h5 class="acod-title">
                                                        <a data-toggle="collapse" href="#{{$k}}" @if($k != 1){ class="collapsed" } @endif data-parent="#{{$k}}">
                                                            <i class="fa fa-pencil"></i> {{ $review->full_name }} </a>
                                                    </h5>
                                                @endif
                                            </div>
                                            <div id="{{$k}}" class="acod-body collapse in">
                                                <div class="acod-content">{!! $review->review !!}</div>
                                            </div>
                                        </div>
                                        @php $k++; @endphp
                                @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        @endif
            <!-- About Us End -->
        </div>
        <div class="clearfix"></div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->

@endsection