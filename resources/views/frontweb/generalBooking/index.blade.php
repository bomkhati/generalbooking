@extends('layouts.frontweb')
@section('title', 'Booking')
@section('content')
    <div class="page-content bg-white">
        <!-- inner page banner -->
        <div class="dez-bnr-inr tb overlay-white-light " style="background-image:url({{asset('storage/frontweb/assets/images/banner.jpg')}});">
            <div class="container">
                <div class="dez-bnr-inr-entry">
                    <h2 class="text-red">Booking</h2>
                    <!-- Breadcrumb row -->
                    <div class="breadcrumb-row">
                        <ul class="list-inline">
                            <li><a href="/">Home</a></li>
                            <li>Booking</li>
                        </ul>
                    </div>
                    <!-- Breadcrumb row END -->
                </div>
            </div>
        </div>
        <!-- inner page banner END -->
        <div class="container">
            <div class="row">
                <!-- Left part start -->
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="p-a30 bg-gray clearfix m-b30 ">
                        <h2>Please complete the form below to make an appointment booking.</h2>
                        <div class="dzFormMsg"></div>

                        <form method="post" autocomplete="off" class="dzForm" action="{{route('save-guest-booking')}}">
                            @csrf
                            <input type="hidden" value="Contact" name="dzToDo">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input name="name" type="text" required="" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input name="email" type="email" class="form-control" required="" placeholder="Email Address">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input name="phone" type="text" required="" class="form-control" placeholder="Phone Number">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 calenderbooking">
                                    <div class="form-group">
                                        <div class="input-group" id="datepicker">
                                            <input type="hidden" id="my_hidden_input" name="booking_date">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9 text-center card" id="booking-slots">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <ul class="time-slots" id="time-slots">
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" id="bottomform">
                                        <div class="input-group">
                                            <select name="treatment">
                                                <option value="n/a" id="select-1545373446907-0">Select treatment</option>
                                                <option value="Mani Bar -> Classic Manicure">Classic Manicure</option>
                                                <option value="Mani Bar -> Deluxe Manicure">Deluxe Manicure</option>
                                                <option value="Hand Gel Polish (Shellac) -> Classic Shellac mani">Classic Shellac mani</option>
                                                <option value="Hand Gel Polish (Shellac) -> Deluxe Shellac mani">Deluxe Shellac mani</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Full set Acrylic / Gel">Full set Acrylic / Gel</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Full set acrylic / Gel Overlay">Full set acrylic / Gel Overlay</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Refill">Refill</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Rebalance" >Rebalance</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Acrylic/ Gel nail removal">Acrylic/ Gel nail removal</option>
                                                <option value="Hand &amp; Foot Acrylic &amp; Gel Nail Enhancements -> Single nail repair">Single nail repair</option>
                                                <option value="Pedi Bar -> Classic Spa Pedicure" >Classic Spa Pedicure</option>
                                                <option value="Pedi Bar -> Deluxe Spa Pedicure" >Deluxe Spa Pedicure</option>
                                                <option value="Foot Gel Polish (Shellac) -> Classic Shellac" >Classic Shellac</option>
                                                <option value="Foot Gel Polish (Shellac) -> Deluxe Shellac Treatment">Deluxe Shellac Treatment</option>
                                                <option value="Foot Gel Polish (Shellac) -> Shellac Removal" >Shellac Removal</option>
                                                <option value="Foot Gel Polish (Shellac) -> Polish change">Polish change</option>
                                                <option value="Foot Gel Polish (Shellac) -> Hand or foot Paraffin Wax treatment">Hand or foot Paraffin Wax treatment</option>
                                                <option value="Nail Design (hand and foot) -> *Nail art / nail">*Nail art / nail</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <textarea name="comment" rows="4" class="form-control" placeholder="Extra Comments..."></textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <button name="submit" type="submit" value="Submit" class="site-button "> <span>Submit</span> </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Left part END -->

            </div>
        </div>
        <!-- contact area END -->
    </div>
    <!-- Content END-->

@endsection
