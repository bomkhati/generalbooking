@extends('layouts.customer')
@section('content')
    <div id="page-title">
        <h2>Welcome, {{ \Auth::user()->name }}</h2>
    </div>

    <div class="row flexthis ">
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        {{formatBookingNumber($approvedBooking->count())}}
                        <h4>Approved Bookings</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        {{formatBookingNumber($pendingBooking->count())}}
                        <h4>Pending Bookings</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="dashboard-box dashboard-box-chart bg-white content-box">
                <div class="content-wrapper">
                    <div class="header">
                        {{formatBookingNumber($cancelledBooking->count())}}
                        <h4>Cancelled  Bookings</h4>
                    </div>
                </div>
                <div class="button-pane">
                    <div class="size-md float-left">
                        <a href="#" title="">
                            View Details
                        </a>
                    </div>
                    <a href="#" class="btn btn-default float-right tooltip-button" data-placement="top" title="View details">
                        <i class="glyph-icon icon-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection
