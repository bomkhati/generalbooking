<?php namespace App\Mail;
use App\Modules\Models\Booking\Booking\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingCreated extends Mailable
{

    use Queueable, SerializesModels;

    public $booking;
    public $customer;

    function __construct(
        Booking $booking, $customer
    ){
        $this->booking = $booking;
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.booking-created')
            ->from('reception@lanailsstudio.com.au', 'La Nails Studio Booking')
            ->subject('New Booking Request');
    }

}