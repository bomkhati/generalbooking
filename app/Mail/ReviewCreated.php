<?php namespace App\Mail;

use App\Modules\Models\Review\Review;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReviewCreated extends Mailable
{

    use Queueable, SerializesModels;

    public $review;
    public $customer;

    function __construct(
        Review $review
    ){
        $this->review = $review;
        $this->customer = Auth::user();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.review-created')
            ->from('reception@lanailsstudio.com.au', 'La Nails Studio Review')
            ->subject('Review Received');
    }

}