<?php namespace App\Mail;
use App\Modules\Models\Booking\Booking\Booking;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingApproved extends Mailable
{

    use Queueable, SerializesModels;

    public $booking;
    public $customer;

    function __construct(
        Booking $booking
    ){
        $this->booking = $booking;
        $this->customer = Auth::user();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.booking-approved')
            ->from('reception@lanailsstudio.com.au', 'La Nails Studio Booking')
            ->subject('Booking Request Approved');
    }

}