<?php

namespace App\Providers;

use App\Modules\Models\SiteSetting\FooterSetting\FooterSetting;
use App\Modules\Models\SiteSetting\Option\GeneralOption;
use App\Modules\Models\SiteSetting\TrackingCode\TrackingCode;
use Illuminate\Support\ServiceProvider;

class FrontEndServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeFrontEndHeaderAndFooter();
    }

    private function composeFrontEndHeaderAndFooter()
    {
        $options = GeneralOption::all();
        view()->composer('layouts.frontweb.header', function ($view) use ($options) {
            $view->with(['options' => $options]);
        });

        $trackingCodes = TrackingCode::all();
        view()->composer('layouts.frontweb.head', function ($view) use ($trackingCodes) {
            $view->with(['trackingCodes' => $trackingCodes]);
        });

        $footerSettings = FooterSetting::all();
        view()->composer('layouts.frontweb.footer', function ($view) use ($footerSettings) {
            $view->with(['footerSettings' => $footerSettings]);
        });

    }

}
