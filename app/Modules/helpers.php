<?php
/**
 * write brief description
 * @param $fullName
 * @return mixed
 */
function getFirstName($fullName){
    $nameArr = explode(' ', $fullName);

    return !empty($nameArr) ? $nameArr[0] : $fullName;
}

/**
 * write brief description
 * @param $fullName
 * @return mixed
 */
function getLastName($fullName){
    $nameArr = explode(' ', $fullName);

    if(!empty($nameArr))
        return strstr($fullName, ' ');

    return '';
}

function getBadge($status){

    switch ($status) {
        case 'active' :
            $badge = 'badge-success';
            break;

        case 'inactive' :
            $badge = 'badge-danger';
            break;

        default :
            $badge = 'badge-success';
            break;

    }

    return $badge;
}

function delete_form($url, $label = 'Delete', $class = '', $title = '', $isButton = true)
{
    $form = Form::open(['method' => 'DELETE', 'url' => $url, 'class' => 'deleteContentForm']);
    if( $isButton ) {
        $form .= "<button type='submit' data-toggle='confirmation' data-title='Confirm delete?' class='$class deleteContentButton' title='$title' data-toggle='tooltip' data-placement='top' data-original-title='$title'>";
        $form .= "<i class='glyph-icon icon-trash'
                                        aria-hidden='true'></i> $label";
        $form .= '</button>';
    }else{
        $form .= "<span class='deleteContentButton'><i
                                        class='glyph-icon icon-trash'
                                        aria-hidden='true'></i> $label </span>";
    }
    return $form .= Form::close();
}

function checkNumberOfBookings($dateTime){
    $bookingsMade = \App\Modules\Models\Booking\Booking\Booking::select('*')->where('time_slots', '=', $dateTime)->get();
    if(!empty($bookingsMade))
        return $bookingsMade->count();
    else
        return 0;
}

function getAllMenus(){
    $availableMenus = \Harimayco\Menu\Models\Menus::select('*')
        ->get();
    if(!empty($availableMenus))
        return $availableMenus;
    else
        return null;
}

function formatBookingNumber($number){
    $formatedNumber = sprintf("%02d", $number);
    return $formatedNumber;
}

function formatDate($date, $format = 'Y-m-d', $fullDate='false')
{
    if($date == '0000-00-00' || empty($date))
        return '-';


    if($fullDate=='true'){
        return date('l d F Y', strtotime($date));
    }
    return date($format, strtotime($date));
}
