<?php namespace App\Modules\Models\Banner;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    private $path = 'uploads/banner';
    protected $table = 'banners';


    protected $fillable = [
        'title', 'image', 'description',  'position', 'status','button_text', 'button_link','button_target'
    ];

    protected $appends = [
        'status_text','button_status_text','thumbnail_path', 'banner_image_path'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function getButtonStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->button_status));
    }

    function getThumbnailPathAttribute(){
        return $this->path.'/'. $this->image;
    }
    function getBannerImagePathAttribute(){
        return $this->path.'/'. $this->image;
    }

}