<?php namespace App\Modules\Models\Page;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    private $path = 'uploads/page';
    use Sluggable;

    protected  $table = 'cms_pages';
    public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = [
        'title','slug', 'description', 'file', 'menu_position','status'
    ];

    protected $appends = [
        'status_text','thumbnail_path','page_image_path'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function getThumbnailPathAttribute(){
        return $this->path.'/'. $this->file;
    }
    function getPageImagePathAttribute(){
        return $this->path.'/'. $this->file;
    }
}