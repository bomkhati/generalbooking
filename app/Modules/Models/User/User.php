<?php

namespace App\Modules\Models\User;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'last_logged_in', 'last_logged_in', 'user_type', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'user_type_text'
    ];


    /**
     * write brief description
     * @return string
     */
    function getUserTypeTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->user_type));
    }
}
