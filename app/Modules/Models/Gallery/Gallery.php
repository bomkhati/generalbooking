<?php namespace App\Modules\Models\Gallery;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    private $path = 'uploads/gallery';
    protected $fillable = [
        'title', 'image'
    ];

    protected $appends = [
        'thumbnail_path','gallery_image_path'
    ];

    function getThumbnailPathAttribute(){
        return $this->path.'/'. $this->image;
    }
    function getGalleryImagePathAttribute(){
        return $this->path.'/'. $this->image;
    }
}