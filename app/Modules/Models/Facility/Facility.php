<?php namespace App\Modules\Models\Facility;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    private $path = 'uploads/facility';
    use Sluggable;

    protected  $table = 'facilities';

    public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = [
        'title','slug', 'description', 'file','status'
    ];

    protected $appends = [
        'status_text','thumbnail_path','facility_image_path'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function getThumbnailPathAttribute(){
        return $this->path.'/'. $this->file;
    }
    function getFacilityImagePathAttribute(){
        return $this->path.'/'. $this->file;
    }

}