<?php namespace App\Modules\Models\Review;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $table = 'reviews';


    protected $fillable = [
        'user_id','full_name','review','status'
    ];

    protected $appends = [
        'status_text'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function customers()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}