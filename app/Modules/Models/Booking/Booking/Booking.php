<?php namespace App\Modules\Models\Booking\Booking;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected  $table = 'bookings';

    protected $fillable = [
        'user_id','time_slots','comment','treatment','status'
    ];

    protected $appends = [
        'status_text'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function customers()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}