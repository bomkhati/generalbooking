<?php namespace App\Modules\Models\Booking\GeneralSetting;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $table = 'booking_settings';


    protected $fillable = [
        'day','start_time','end_time','space_available','booking_interval','total_booking','isHoliday'
    ];

}