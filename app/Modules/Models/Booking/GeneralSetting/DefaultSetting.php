<?php namespace App\Modules\Models\Booking\GeneralSetting;

use Illuminate\Database\Eloquent\Model;

class DefaultSetting extends Model
{
    protected $table = 'booking_default_settings';


    protected $fillable = [
        'day','start_time','end_time','space_available','booking_interval','total_booking'
    ];

}