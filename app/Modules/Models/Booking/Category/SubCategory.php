<?php namespace App\Modules\Models\Booking\Category;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';

    protected $fillable = [
        'sub_category','category_id','price'
    ];


    function category(){
        return $this->belongsTo('App\Modules\Models\Booking\Category\Category','category_id');
    }

}