<?php namespace App\Modules\Models\Booking\Category;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'category'
    ];

}