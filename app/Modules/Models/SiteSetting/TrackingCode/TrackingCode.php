<?php namespace App\Modules\Models\SiteSetting\TrackingCode;

use Illuminate\Database\Eloquent\Model;

class TrackingCode extends Model
{
    protected $table = 'tracking_codes';


    protected $fillable = [
        'keyword','keyword_description','facebook_script','facebook_noscript','google_analytics_script','google_analytics_noscript'
    ];

}