<?php namespace App\Modules\Models\SiteSetting\FooterSetting;

use Illuminate\Database\Eloquent\Model;

class FooterSetting extends Model
{
    private $path = 'uploads/footerSetting';
    protected $table = 'footer_settings';


    protected $fillable = [
        'location','opening_hours','copyright','file','status'
    ];

    protected $appends = [
        'status_text','thumbnail_path','setting_image_path'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

    function getThumbnailPathAttribute(){
        return $this->path.'/'. $this->file;
    }

    function getSettingImagePathAttribute(){
        return $this->path.'/'. $this->file;
    }
}