<?php namespace App\Modules\Models\SiteSetting\Maintenance;

use Illuminate\Database\Eloquent\Model;

class SiteMaintenance extends Model
{


    protected $table = 'site_maintenance';


    protected $fillable = [
        'title', 'status',
    ];

    protected $appends = [
        'status_text'
    ];

    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }

}