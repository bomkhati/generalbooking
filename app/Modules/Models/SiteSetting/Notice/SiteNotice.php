<?php namespace App\Modules\Models\SiteSetting\Notice;


use Illuminate\Database\Eloquent\Model;

class SiteNotice extends Model
{

 protected $fillable = [
        'title', 'notice', 'status'

    ];

    protected $appends = [
        'status_text'
    ];


    function getStatusTextAttribute(){
        return ucwords(str_replace('_', ' ', $this->status));
    }
}