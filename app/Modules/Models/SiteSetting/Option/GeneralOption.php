<?php namespace App\Modules\Models\SiteSetting\Option;

use Illuminate\Database\Eloquent\Model;

class GeneralOption extends Model
{
    private $path = 'uploads/logo';

    protected $fillable = [
        'company_name', 'email_address', 'contact_number','location','image'
    ];

    protected $appends = [
        'thumbnail_path','image_path'
    ];

    function getImagePathAttribute(){
        return $this->path.'/'. $this->image;
    }

    function getThumbnailPathAttribute(){
        return $this->path.'/thumb/'. $this->image;
    }
}