<?php namespace App\Modules\Services\Facility;

use App\Modules\Models\Facility\Facility;
use App\Modules\Services\Service;

class FacilityService extends Service
{
    protected $facility;

    public function __construct(Facility $facility){
        $this->facility = $facility;
    }

    /**
     * Create new Facility
     *
     * @param array $data
     * @return Facility|null
     */
    public function create(array $data)
    {
        try {
            $file = $data['file'];
            if(!empty($file)){
                $this->uploadPath = 'uploads/facility';
                $fileName = $this->upload($file);
                $data['file'] = $fileName;
            }else{
                unset($data['file']);
            }
            $facility = $this->facility->create($data);
            return $facility;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all Facility
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->facility->paginate($filter['limit']);
    }

    /**
     * Get all Facility
     *
     * @return Collection
     */
    public function all()
    {
        return $this->facility->all();
    }

       /**
     * Get a Facility
     *
     * @param $facilityId
     * @return Facility |null
     */
    public function find($facilityId)
    {
        try {
            return $this->facility->find($facilityId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Facility
     * @param $facilityId
     * @param array $data
     * @return bool
     */
    public function update($facilityId, array $data)
{
    try {
        $pid = $this->facility->find($facilityId);
        if(isset($data['file']))
            $file = $data['file'];
        if(!empty($file)){
            $this->uploadPath = 'uploads/facility';
            $fileName = $this->upload($file);
            $data['file'] = $fileName;
            $this->__deleteImages($pid);
        }
        else {
            unset($data['file']);
        }

        $facility = $pid->update($data);
        //$this->logger->info(' created successfully', $data);

        return $facility;
    } catch (Exception $e) {
        //$this->logger->error($e->getMessage());
        return false;
    }
}

    /**
     * Delete a Facility
     *
     * @param Id
     * @return bool
     */
    public function delete($serviceId)
    {
        try {
            $facility = $this->facility->find($serviceId);

            return $facility->delete();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    private function __deleteImages($facility){
        try{
            if(is_file($facility->image_path))
                unlink($facility->image_path);

            if(is_file($facility->thumbnail_path))
                unlink($facility->thumbnail_path);
        }catch (\Exception $e){

        }
    }


    public function getByName($name){
        return $this->facility->whereName($name);
    }

    public function getBySlug($slug){
        try{
            return $this->facility->whereSlug($slug)->first();
        }catch (\Exception $e){
            return new $this->facility();
        }

    }
}