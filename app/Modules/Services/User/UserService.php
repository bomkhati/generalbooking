<?php namespace App\Modules\Services\User;

use App\Modules\Services\Service;
use App\User;
use Carbon\Carbon;

class UserService extends Service
{
    protected $user;

    public function __construct(
        User $user
    ){
        $this->user = $user;
    }

    /**
     * Create new User
     *
     * @param array $data
     * @return User|null
     */
    public function create(array $data)
    {
        try {
            if(!isset($data['customer_type']) && $data['user_type']=='customer')
                $data['customer_type'] = 'registered';

            $data['password'] = bcrypt($data['password']);
            $data['last_logged_in'] = Carbon::now();
            $user = $this->user->create($data);
            return $user;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all User
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->user->whereUserType('customer')->orderBy('id','asc')->paginate($filter['limit']);
    }

    /**
     * Get all User
     *
     * @return Collection
     */
    public function all()
    {
        return $this->user->all();
    }

    /**
     * Get a User
     *
     * @param $userId
     * @return User |null
     */
    public function find($userId)
    {
        try {
            return $this->user->find($userId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the User
     * @param $userId
     * @param array $data
     * @return bool
     */
    public function update($userId, array $data)
    {
        try {
            $user= $this->user->find($userId);
            $data['password'] = bcrypt($data['password']);
            $data['last_logged_in'] = Carbon::now();
            $user = $user->update($data);
            //$this->logger->info(' created successfully', $data);

            return $user;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a User
     *
     * @param Id
     * @return bool
     */
    public function delete($userId)
    {
        try {
            $user = $this->user->find($userId);

            return $user->delete();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    public function getByName($username){
        return $this->user->whereName($username);
    }

    public function getByEmail($email){
        return $this->user->whereEmail($email)->first();
    }
}