<?php namespace App\Modules\Services\Option;

use App\Modules\Models\SiteSetting\Option\GeneralOption;
use App\Modules\Services\Service;
use Illuminate\Database\QueryException;

class GeneralOptionService extends Service
{
    protected $option;

    public function __construct(
        GeneralOption $option
    )
    {
        $this->option = $option;
    }

    /**
     * Create new General Option
     *
     * @param array $data
     * @return General Option|null
     */
    public function create(array $data)
    {
        try {
            //now try to upload the file
            if(isset($data['file']))
                $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/logo';
                $fileName = $this->upload($file);

                $data['image'] = $fileName;
            }else{
                unset($data['image']);
            }
            $option = $this->option->create($data);
            return $option;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all Parent General Option
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->option->paginate($filter['limit']);
    }

    /**
     * Get all Parent General Option
     *
     * @return Collection
     */
    public function all()
    {
        return $this->option->all();
    }

    /**
     * Get a General Option
     *
     * @param $optionId
     * @return parent General Option |null
     */
    public function find($optionId)
    {
        try {
            return $this->option->find($optionId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the General Option
     * @param $optionId
     * @param array $data
     * @return bool
     */
    public function update($optionId, array $data)
    {
        try {
            $p_pid = $this->option->find($optionId);
            if(!empty($data['file']))
                $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/logo';
                $fileName = $this->upload($file, '175', '175');

                $data['image'] = $fileName;

                $this->__deleteImages($p_pid);
            }else{
                unset($data['image']);
            }
            $option = $p_pid->update($data);
            //$this->logger->info(' created successfully', $data);

            return $option;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a General Option
     *
     * @param Id
     * @return bool
     */
    public function delete($optionId)
    {
        try {
            $option = $this->option->find($optionId);

            return $option->delete();

        } catch (QueryException $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */

    private function __deleteImages($option){
        try{
            if(is_file($option->image_path))
                unlink($option->image_path);

            if(is_file($option->thumbnail_path))
                unlink($option->thumbnail_path);
        }catch (\Exception $e){

        }
    }

}