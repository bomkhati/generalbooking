<?php namespace App\Modules\Services\Booking;

use App\Modules\Models\Booking\Category\Category;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use App\Modules\Services\Service;

class CategoryService extends Service
{
    protected $category;


    public function __construct(
        Category $category
    ){
        $this->category = $category;

    }

    /**
     * Create new Category
     *
     * @param array $data
     * @return Category|null
     */
    public function create(array $data)
    {
        try {
            $category = $this->category->create($data);

            return $category;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all Category
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->category->paginate($filter['limit']);
    }

    /**
     * Get all Category
     *
     * @return Collection
     */
    public function all()
    {
        return $this->category->get();
    }

    /**
     * Get a Category
     *
     * @param $categoryId
     * @return Category |null
     */
    public function find($categoryId)
    {
        try {
            return $this->category->find($categoryId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Category
     * @param $categoryId
     * @param array $data
     * @return bool
     */
    public function update($categoryId, array $data)
    {
        try {
            $st = $this->category->find($categoryId);
            $category = $st->update($data);
            return $category;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Delete a Category
     *
     * @param Id
     * @return bool
     */
    public function delete($categoryId)
    {
        try {
            $category = $this->category->find($categoryId);
            return $category->delete();

        } catch (QueryException $e) {

            return false;

        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    public function getByName($name){
        return $this->category->whereName($name);
    }

    public function getBySlug($slug){
        return $this->category->whereSlug($slug)->first();
    }





}