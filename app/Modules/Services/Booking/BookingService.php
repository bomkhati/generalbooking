<?php namespace App\Modules\Services\Booking;

use App\Mail\BookingApproved;
use App\Mail\BookingCreated;
use App\Modules\Models\Booking\Booking\Booking;
use App\Modules\Services\Service;
use App\Modules\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BookingService extends Service
{
    protected $booking;
    protected $user;


    public function __construct(
       Booking $booking, UserService $user
    ){
        $this->booking = $booking;
        $this->user = $user;

    }
    public function create(array $data)
    {
        try {
            if (!isset($data['user_id']))
                $data['user_id'] = Auth::user()->id;

            $user = $this->user->find($data['user_id']);

            $time = $data['booking_time'];
            $day = $data['booking_date'];
            $bookingDay = $day.' '.$time;
            $date = Carbon::parse($bookingDay)->format('Y-m-d h:i:s');
            $data['time_slots'] = $date;
            $booking = $this->booking->create($data);
            $email = $user->email;
            if (!empty($booking)) {
                try{
                    Mail::to($email)->send(new BookingCreated($booking,$user));
                }catch (Exception $e){

                }
            }
            return $booking;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->booking->get()->first();
    }

    /**
     * Paginate all DefaultSetting
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->booking->orderBy('created_at','desc')->paginate($filter['limit']);
    }

    /**
     * Get all DefaultSetting
     *
     * @return Collection
     */
    public function all()
    {
        return $this->booking->all();
    }

    /**
     * Get a DefaultSetting
     *
     * @param $bookingId
     * @return Booking |null
     */
    public function find($bookingId)
    {
        try {
            return $this->booking->find($bookingId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the DefaultSetting
     * @param $bookingId
     * @param array $data
     * @return bool
     */

    public function update($bookingId, array $data)
    {
        try {
            $booking = $this->booking->find($bookingId);
            $booking = $booking->update($data);
            return $booking;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a DefaultSetting
     *
     * @param Id
     * @return bool
     */
    public function delete($bookingId)
    {
        try {
            $booking = $this->booking->find($bookingId);
            return $booking->delete();

        } catch (Exception $e) {

            return false;

        }
    }

    public function getLatestBooking()
    {
        $bookings = $this->booking
            ->orderBy('created_at','desc')->paginate('5')
            ->first();
        return $bookings;
    }

    public function customerTotalBooking($id)
    {
        $bookings = $this->booking->whereUserId($id)
            ->whereIn('status', ['approved', 'pending'])
            ->orderBy('created_at', 'desc')->paginate(25);
        return $bookings;

    }

    public function customerApprovedBooking($id)
    {
        $bookings = $this->booking->whereUserId($id)
            ->whereIn('status', ['approved'])
            ->orderBy('created_at', 'desc')->paginate(25);
        return $bookings;

    }

    public function customerPendingBooking($id)
    {
        $bookings = $this->booking->whereUserId($id)
            ->whereIn('status', ['pending'])
            ->orderBy('created_at', 'desc')->paginate(25);
        return $bookings;

    }

    public function customerCancelledBooking($id)
    {
        $bookings = $this->booking->whereUserId($id)
            ->whereIn('status', ['cancelled'])
            ->orderBy('created_at', 'desc')->paginate(25);
        return $bookings;

    }

}