<?php namespace App\Modules\Services\Booking;

use App\Modules\Models\Booking\GeneralSetting\DefaultSetting;
use App\Modules\Services\Service;

class DefaultSettingService extends Service
{
    protected $defaultSetting;


    public function __construct(
       DefaultSetting $defaultSetting
    ){
        $this->defaultSetting = $defaultSetting;

    }
    public function create(array $data)
    {
        try {
            $defaultSetting = $this->defaultSetting->create($data);
            return $defaultSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->defaultSetting->get()->first();
    }

    public function getAvailableSlotsByDay($day){
        return $this->defaultSetting->where('day', '=', $day)->first();
    }
    /**
     * Paginate all DefaultSetting
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->defaultSetting->paginate($filter['limit']);
    }

    /**
     * Get all DefaultSetting
     *
     * @return Collection
     */
    public function all()
    {
        return $this->defaultSetting->all();
    }

    /**
     * Get a DefaultSetting
     *
     * @param $defaultSettingId
     * @return Session |null
     */
    public function find($defaultSettingId)
    {
        try {
            return $this->defaultSetting->find($defaultSettingId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the DefaultSetting
     * @param $defaultSettingId
     * @param array $data
     * @return bool
     */

    public function update($defaultSettingId, array $data)
    {
        try {
            $defaultSetting = $this->defaultSetting->find($defaultSettingId);
            $defaultSetting = $defaultSetting->update($data);
            return $defaultSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a DefaultSetting
     *
     * @param Id
     * @return bool
     */
    public function delete($defaultSettingId)
    {
        try {
            $defaultSetting = $this->defaultSetting->find($defaultSettingId);
            return $defaultSetting->delete();

        } catch (Exception $e) {

            return false;

        }
    }



}