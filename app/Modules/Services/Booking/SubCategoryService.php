<?php namespace App\Modules\Services\Booking;

use App\Modules\Models\Booking\Category\Category;
use App\Modules\Models\Booking\Category\SubCategory;
use App\Modules\Services\Service;

class SubCategoryService extends Service
{
    protected $category;
    protected $subcategory;

    public function __construct(Category $category, SubCategory $subCategory)
    {
        $this->category = $category;
        $this->subcategory = $subCategory;
    }

    /**
     * Create new Sub Category
     *
     * @param array $data
     * @return Sub Category|null
     */
    public function create(array $data)
    {
        try {
            $subcategory = $this->subcategory->create($data);
            return $subcategory;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all Sub Category
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->subcategory->paginate($filter['limit']);
    }

    /**
     * Get all Sub Category
     *
     * @return Collection
     */
    public function all()
    {
        return $this->subcategory
            ->where('status','active')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * Get a Sub Category
     *
     * @param $subCategoryId
     * @return ExamSession |null
     */
    public function find($subCategoryId)
    {
        try {
            return $this->subcategory->find($subCategoryId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Session
     * @param $sessionId
     * @param array $data
     * @return bool
     */
    public function update($subCategoryId, array $data)
    {
        try {
            $st = $this->subcategory->find($subCategoryId);
            if(isset($data['status']))
                $data['status'] = $data['status'] == 'on' ? 'active' : 'in_active';
            else
                $data['status']='in_active';
            $faq = $st->update($data);
            //$this->logger->info(' created successfully', $data);


            return $faq;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Sub Category
     *
     * @param Id
     * @return bool
     */
    public function delete($faqId)
    {
        try {
            $subcategory = $this->subcategory->find($faqId);
            return $subcategory->delete();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    public function getByName($name){
        return $this->subcategory->whereName($name);
    }

}