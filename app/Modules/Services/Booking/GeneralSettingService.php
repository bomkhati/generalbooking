<?php namespace App\Modules\Services\Booking;

use App\Modules\Models\Booking\GeneralSetting\GeneralSetting;
use App\Modules\Services\Service;

class GeneralSettingService extends Service
{
    protected $generalSetting;


    public function __construct(
        GeneralSetting $generalSetting
    ){
        $this->generalSetting = $generalSetting;

    }
    public function create(array $data)
    {
        try {
            $generalSetting = $this->generalSetting->create($data);
            return $generalSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->generalSetting->get()->first();
    }

    /**
     * Paginate all GeneralSetting
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->generalSetting->paginate($filter['limit']);
    }

    /**
     * Get all GeneralSetting
     *
     * @return Collection
     */
    public function all()
    {
        return $this->generalSetting->all();
    }

    /**
     * Get a GeneralSetting
     *
     * @param $generalSettingId
     * @return Session |null
     */
    public function find($generalSettingId)
    {
        try {
            return $this->generalSetting->find($generalSettingId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the GeneralSetting
     * @param $generalSettingId
     * @param array $data
     * @return bool
     */

    public function update($generalSettingId, array $data)
    {
        try {
            $generalSetting = $this->generalSetting->find($generalSettingId);
            $generalSetting = $generalSetting->update($data);
            return $generalSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a GeneralSetting
     *
     * @param Id
     * @return bool
     */
    public function delete($generalSettingId)
    {
        try {
            $generalSetting = $this->generalSetting->find($generalSettingId);
            return $generalSetting->delete();

        } catch (Exception $e) {

            return false;

        }
    }

    public function getAvailableSlotsByDay($day){
        return $this->generalSetting->where('day', '=', $day)->first();
    }



}