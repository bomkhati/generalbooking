<?php namespace App\Modules\Services\Page;

use App\Modules\Models\Page\Page;
use App\Modules\Services\Service;

class PageService extends Service
{
    protected $page;

    public function __construct(
        Page $page
    ){
        $this->page = $page;
    }

    /**
     * Create new Post
     *
     * @param array $data
     * @return Page|null
     */
    public function create(array $data)
    {
        //dd($data);
        try {
            //now try to upload the file
            $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/page';
                $fileName = $this->upload($file);

                $data['file'] = $fileName;
            }else{
                unset($data['file']);
            }
            $page = $this->page->create($data);
            return $page;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all Post
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->page->paginate($filter['limit']);
    }

    /**
     * Get all Post
     *
     * @return Collection
     */
    public function all()
    {
        return $this->page->all();
    }
    
       /**
     * Get a Post
     *
     * @param $pageId
     * @return Post |null
     */
    public function find($pageId)
    {
        try {
            return $this->page->find($pageId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Post
     * @param $pageId
     * @param array $data
     * @return bool
     */
    public function update($pageId, array $data)
    {
        try {
            $st = $this->page->find($pageId);
            if(isset($data['file']))
                $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/page';
                $fileName = $this->upload($file);

                $data['file'] = $fileName;

                $this->__deleteImages($st);
            }
            else {
                unset($data['image']);
            }
            $course = $st->update($data);
            //$this->logger->info(' created successfully', $data);

            return $course;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Post
     *
     * @param Id
     * @return bool
     */
    public function delete($pageId)
    {
        try {
            $page = $this->page->find($pageId);

            return $page->delete();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    private function __deleteImages($page){
        try{
            if(is_file($page->image_path))
                unlink($page->image_path);

            if(is_file($page->thumbnail_path))
                unlink($page->thumbnail_path);
        }catch (\Exception $e){

        }
    }

    public function getpageById($id){
        return $this->page->where('id',$id);
    }

    public function getByName($name){
        return $this->page->whereName($name);
    }

    public function getBySlug($slug){
        try{
            return $this->page->whereSlug($slug)->get();
        }catch (\Exception $e){
            return new $this->page();
        }

    }
}