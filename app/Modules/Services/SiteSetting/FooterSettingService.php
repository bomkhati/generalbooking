<?php namespace App\Modules\Services\SiteSetting;

use App\Modules\Models\SiteSetting\FooterSetting\FooterSetting;
use App\Modules\Services\Service;

class FooterSettingService extends Service
{
    protected $footerSetting;


    public function __construct(
       FooterSetting $footerSetting
    ){
        $this->footerSetting = $footerSetting;

    }
    public function create(array $data)
    {
        try {
            $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/footerSetting';
                $fileName = $this->upload($file);

                $data['file'] = $fileName;
            }else{
                unset($data['file']);
            }
            $footerSetting = $this->footerSetting->create($data);
            return $footerSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->footerSetting->get()->first();
    }

    /**
     * Paginate all FooterSetting
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->footerSetting->paginate($filter['limit']);
    }

    /**
     * Get all FooterSetting
     *
     * @return Collection
     */
    public function all()
    {
        return $this->footerSetting->all();
    }

    /**
     * Get a FooterSetting
     *
     * @param $footerSettingId
     * @return Session |null
     */
    public function find($footerSettingId)
    {
        try {
            return $this->footerSetting->find($footerSettingId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the FooterSetting
     * @param $footerSettingId
     * @param array $data
     * @return bool
     */

    public function update($footerSettingId, array $data)
    {
        try {
            $footerSetting = $this->footerSetting->find($footerSettingId);
            if(isset($data['file']))
                $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/footerSetting';
                $fileName = $this->upload($file);

                $data['file'] = $fileName;

                $this->__deleteImages($footerSetting);
            }
            else {
                unset($data['file']);
            }
            $footerSetting = $footerSetting->update($data);
            return $footerSetting;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a FooterSetting
     *
     * @param Id
     * @return bool
     */
    public function delete($footerSettingId)
    {
        try {
            $footerSetting = $this->footerSetting->find($footerSettingId);
            return $footerSetting->delete();

        } catch (Exception $e) {

            return false;

        }
    }

    private function __deleteImages($footerSetting){
        try{
            if(is_file($footerSetting->image_path))
                unlink($footerSetting->image_path);

            if(is_file($footerSetting->thumbnail_path))
                unlink($footerSetting->thumbnail_path);
        }catch (\Exception $e){

        }
    }


}