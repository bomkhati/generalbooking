<?php namespace App\Modules\Services\SiteSetting;

use App\Modules\Models\SiteSetting\TrackingCode\TrackingCode;
use App\Modules\Services\Service;

class TrackingCodeService extends Service
{
    protected $trackingCode;


    public function __construct(
       TrackingCode $trackingCode
    ){
        $this->trackingCode = $trackingCode;

    }
    public function create(array $data)
    {
        try {
            $trackingCode = $this->trackingCode->create($data);
            return $trackingCode;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->trackingCode->get()->first();
    }

    /**
     * Paginate all TrackingCode
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->trackingCode->paginate($filter['limit']);
    }

    /**
     * Get all TrackingCode
     *
     * @return Collection
     */
    public function all()
    {
        return $this->trackingCode->all();
    }

    /**
     * Get a TrackingCode
     *
     * @param $trackingCodeId
     * @return TrackingCode |null
     */
    public function find($trackingCodeId)
    {
        try {
            return $this->trackingCode->find($trackingCodeId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the TrackingCode
     * @param $trackingCodeId
     * @param array $data
     * @return bool
     */

    public function update($trackingCodeId, array $data)
    {
        try {
            $trackingCode = $this->trackingCode->find($trackingCodeId);
            $trackingCode = $trackingCode->update($data);
            return $trackingCode;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a TrackingCode
     *
     * @param Id
     * @return bool
     */
    public function delete($trackingCodeId)
    {
        try {
            $trackingCode = $this->trackingCode->find($trackingCodeId);
            return $trackingCode->delete();

        } catch (Exception $e) {

            return false;

        }
    }

}