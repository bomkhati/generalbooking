<?php namespace App\Modules\Services\SiteSetting;

use App\Modules\Models\SiteSetting\Maintenance\SiteMaintenance;
use App\Modules\Services\Service;

class SiteMaintenanceService extends Service
{
    protected $siteMaintenance;


    public function __construct(
        SiteMaintenance $siteMaintenance
    ){
        $this->siteMaintenance = $siteMaintenance;

    }



    /**
     * Paginate all Session
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->siteMaintenance->paginate($filter['limit']);
    }

    /**
     * Get all Session
     *
     * @return Collection
     */
    public function all()
    {
        return $this->siteMaintenance->all();
    }

    /**
     * Get a Session
     *
     * @param $sessionId
     * @return Session |null
     */
    public function find($categoryId)
    {
        try {
            return $this->siteMaintenance->find($categoryId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Session
     * @param $sessionId
     * @param array $data
     * @return bool
     */
    public function update($settingId, array $data)
    {
        try {
            $st = $this->siteMaintenance->find($settingId);
            if(isset($data['status']))
            $data['status'] = $data['status'] == 'on' ? 'active' : 'in_active';
            else
            $data['status'] ='in_active';
            $maintenance = $st->update($data);
            //$this->logger->info(' created successfully', $data);


            return $maintenance;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Session
     *
     * @param Id
     * @return bool
     */
    public function delete($settingId)
    {
        try {
            $siteMaintenance = $this->siteMaintenance->find($settingId);
            return $siteMaintenance->delete();

        } catch (Exception $e) {

            return false;

        }
    }



}