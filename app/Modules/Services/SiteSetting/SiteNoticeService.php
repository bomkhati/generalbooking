<?php namespace App\Modules\Services\SiteSetting;

use App\Modules\Models\SiteSetting\Notice\SiteNotice;
use App\Modules\Services\Service;

class SiteNoticeService extends Service
{
    protected $siteNotice;


    public function __construct(
        SiteNotice $siteNotice
    ){
        $this->siteNotice = $siteNotice;

    }

    /**
     * Create new Session
     *
     * @param array $data
     * @return ExamSession|null
     */
    public function create(array $data)
    {
        try {
            $siteNotice = $this->siteNotice->create($data);
            return $siteNotice;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all Session
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->siteNotice->paginate($filter['limit']);
    }

    /**
     * Get all Session
     *
     * @return Collection
     */
    public function all()
    {
        return $this->siteNotice->all();
    }


    /**
     * Update the Session
     * @param $sessionId
     * @param array $data
     * @return bool
     */
    public function update($settingId, array $data)
    {
        try {
            $st = $this->siteNotice->find($settingId);

            $category = $st->update($data);
            //$this->logger->info(' created successfully', $data);


            return $category;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Session
     *
     * @param Id
     * @return bool
     */
    public function delete($settingId)
    {
        try {
            $siteNotice = $this->siteNotice->find($settingId);
            return $siteNotice->delete();

        } catch (Exception $e) {

            return false;

        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    public function getByName($name){
        return $this->siteNotice->whereName($name);
    }


    public function find($settingId)
    {
        try {
            return $this->siteNotice->find($settingId);
        } catch (Exception $e) {
            return null;
        }
    }





}