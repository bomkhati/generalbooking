<?php namespace App\Modules\Services\Review;

use App\Mail\ReviewCreated;
use App\Modules\Models\Review\Review;
use App\Modules\Services\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ReviewService extends Service
{
    protected $review;


    public function __construct(
       Review $review
    ){
        $this->review = $review;

    }
    public function create(array $data)
    {
        try {
            $user_type = Auth::user()->user_type;
            if($user_type != 'customer'){

                $data['status'] = 'approved';
                $review = $this->review->create($data);

            }else{

                $data['user_id'] = Auth::user()->id;
                $review = $this->review->create($data);
                $email = Auth::user()->email;

                if (!empty($review)) {
                    try{
                        Mail::to($email)->send(new ReviewCreated($review));
                    }catch (Exception $e){
                    }
                }
            }
            return $review;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    public function get(){
        return $this->review->get()->first();
    }

    /**
     * Paginate all DefaultSetting
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->review->orderBy('created_at','asc')->paginate($filter['limit']);
    }

    /**
     * Get all DefaultSetting
     *
     * @return Collection
     */
    public function all()
    {
        return $this->review->all();
    }

    /**
     * Get a DefaultSetting
     *
     * @param $reviewId
     * @return Review |null
     */
    public function find($reviewId)
    {
        try {
            return $this->review->find($reviewId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Review
     * @param $reviewId
     * @param array $data
     * @return bool
     */

    public function update($reviewId, array $data)
    {
        try {
            $review = $this->review->find($reviewId);
            $review = $review->update($data);
            return $review;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a DefaultSetting
     *
     * @param Id
     * @return bool
     */
    public function delete($reviewId)
    {
        try {
            $review = $this->review->find($reviewId);
            return $review->delete();

        } catch (Exception $e) {

            return false;

        }
    }

    public function customerReview($id)
    {
        $review = $this->review->whereUserId($id)
            ->whereIn('status', ['approved', 'pending'])
            ->orderBy('created_at', 'desc')->paginate(25);
        return $review;

    }

    public function getApprovedReview()
    {
        return $this->review->whereStatus('approved')->get();
    }
}