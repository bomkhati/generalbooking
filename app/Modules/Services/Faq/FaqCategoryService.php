<?php namespace App\Modules\Services\Faq;

use App\Modules\Models\Faq\FaqCategory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use App\Modules\Services\Service;

class FaqCategoryService extends Service
{
    protected $faqCategory;


    public function __construct(
       FaqCategory $faqCategory
    ){
        $this->faqCategory = $faqCategory;

    }

    /**
     * Create new Session
     *
     * @param array $data
     * @return ExamSession|null
     */
    public function create(array $data)
    {
        try {
            if(isset($data['status']))
                $data['status'] = $data['status'] == 'on' ? 'active' : 'in_active';
            else
                $data['status']='in_active';
            $category = $this->faqCategory->create($data);

            return $category;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Paginate all Session
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->faqCategory->paginate($filter['limit']);
    }

    /**
     * Get all Session
     *
     * @return Collection
     */
    public function all()
    {
        return $this->faqCategory
            ->where('status','active')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * Get a Session
     *
     * @param $sessionId
     * @return Session |null
     */
    public function find($categoryId)
    {
        try {
            return $this->faqCategory->find($categoryId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Session
     * @param $sessionId
     * @param array $data
     * @return bool
     */
    public function update($categoryId, array $data)
    {
        try {
            $st = $this->faqCategory->find($categoryId);
            if(isset($data['status']))
                $data['status'] = $data['status'] == 'on' ? 'active' : 'in_active';
            else
                $data['status']='in_active';
            $category = $st->update($data);
            //$this->logger->info(' created successfully', $data);


            return $category;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Session
     *
     * @param Id
     * @return bool
     */
    public function delete($categoryId)
    {
        try {
            $category = $this->faqCategory->find($categoryId);
            return $category->delete();

        } catch (QueryException $e) {

            return false;

        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    public function getByName($name){
        return $this->faqCategory->whereName($name);
    }

    public function getBySlug($slug){
        return $this->faqCategory->whereSlug($slug)->first();
    }



}