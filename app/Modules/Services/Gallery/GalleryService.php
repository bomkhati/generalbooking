<?php namespace App\Modules\Services\Gallery;

use App\Modules\Models\Gallery\Gallery;
use App\Modules\Services\Service;

class GalleryService extends Service
{
    protected $gallery;

    public function __construct(Gallery $gallery){
        $this->gallery = $gallery;
    }

    /**
     * Create new Facility
     *
     * @param array $data
     * @return Facility|null
     */
    public function create(array $data)
    {
        try {
            $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/gallery';
                $fileName = $this->upload($file);

                $data['image'] = $fileName;
            }else{
                unset($data['image']);
            }
            $gallery = $this->gallery->create($data);
            return $gallery;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all Facility
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 25;

        return $this->gallery->paginate($filter['limit']);
    }

    /**
     * Get all Facility
     *
     * @return Collection
     */
    public function all()
    {
        return $this->gallery->all();
    }

    /**
     * Get a Facility
     *
     * @param $galleryId
     * @return Facility |null
     */
    public function find($galleryId)
    {
        try {
            return $this->gallery->find($galleryId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update the Facility
     * @param $galleryId
     * @param array $data
     * @return bool
     */
    public function update($galleryId, array $data)
    {
        try {
            $st = $this->gallery->find($galleryId);
            if(isset($data['file']))
                $file = $data['file'];

            if(!empty($file)){
                $this->uploadPath = 'uploads/gallery';
                $fileName = $this->upload($file);

                $data['image'] = $fileName;

                $this->__deleteImages($st);
            }
            else {
                unset($data['image']);
            }
            $gallery = $st->update($data);
            //$this->logger->info(' created successfully', $data);


            return $gallery;
        } catch (Exception $e) {
            //$this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * Delete a Facility
     *
     * @param Id
     * @return bool
     */
    public function delete($serviceId)
    {
        try {
            $gallery = $this->gallery->find($serviceId);

            return $gallery->delete();

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * write brief description
     * @param $name
     * @return mixed
     */
    private function __deleteImages($gallery){
        try{
            if(is_file($gallery->image_path))
                unlink($gallery->image_path);

            if(is_file($gallery->thumbnail_path))
                unlink($gallery->thumbnail_path);
        }catch (\Exception $e){

        }
    }


    public function getByName($name){
        return $this->gallery->whereName($name);
    }

    public function getBySlug($slug){
        try{
            return $this->gallery->whereSlug($slug)->first();
        }catch (\Exception $e){
            return new $this->gallery();
        }

    }

}