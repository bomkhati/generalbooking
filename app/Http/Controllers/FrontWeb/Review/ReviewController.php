<?php

namespace App\Http\Controllers\FrontWeb\Review;

use App\Modules\Services\Review\ReviewService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController  extends Controller
{
    protected $review;

    public function __construct(ReviewService  $review)
    {
        $this->middleware('customer');
        $this->review = $review;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
        public function index()
        {
            $approvedReviews = $this->review->getApprovedReview();
            return view('frontweb.review.index',compact('approvedReviews'));
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
        {
            if ($this->review->create($request->all())) {
                return redirect()->route('my-review.index')->with('success', 'Review submitted successfully.');
            }

            return redirect()->route('customer.dashboard')->with('error', 'Review could not be submitted.');
        }
}
