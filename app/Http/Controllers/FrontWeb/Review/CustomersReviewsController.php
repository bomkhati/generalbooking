<?php

namespace App\Http\Controllers\Frontweb\Review;

use App\Http\Controllers\Controller;
use App\Modules\Services\Review\ReviewService;

class CustomersReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $review;

    public function __construct(ReviewService $review)
    {
        $this->review = $review;
    }

    public function index()
    {
        $reviews = $this->review->getApprovedReview();
        return view('frontweb.customerReview.index', compact('reviews'));
    }

}
