<?php

namespace App\Http\Controllers\FrontWeb\Booking;

use App\Modules\Services\Booking\BookingService;
use App\Modules\Services\Booking\DefaultSettingService;
use App\Modules\Services\Booking\GeneralSettingService;
use App\Modules\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class GuestBookingController extends Controller
{
    protected $defaultBooking;
    protected $specialBooking;
    protected $booking;
    protected $user;

    public function __construct(DefaultSettingService  $defaultSetting,
                                GeneralSettingService $generalSettingService,
                                BookingService $booking,
                                UserService $user)
    {

        $this->defaultBooking = $defaultSetting;
        $this->specialBooking = $generalSettingService;
        $this->booking = $booking;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontweb.booking.calendar');
    }


    public function saveBooking(Request $request)
    {
        $user = $this->user->getByEmail($request->get('email'));
        if(empty($user))
        {
            $userData['name'] = $request->get('name');
            $userData['email'] = $request->get('email');
            $userData['phone'] = $request->get('phone');
            $userData['user_type'] = 'customer';
            $userData['customer_type'] = 'guest';
            $userData['password'] = $request->get('Forgot911!');
            $user = $this->user->create($userData);
        }



        $bookingData['user_id'] = $user->id;
        $bookingData['booking_date'] = $request->get('booking_date');
        $bookingData['booking_time'] = $request->get('booking_time');
        $bookingData['treatment'] = $request->get('treatment');
        $bookingData['comment'] = $request->get('comment');

        if ($this->booking->create($bookingData)) {
            $msg = "Your booking has been made. ";
            return view('frontweb.booking.guest-message')->with('msg', 'success');
        }
        return view('frontweb.booking.guest-message')->with('msg', 'error');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
