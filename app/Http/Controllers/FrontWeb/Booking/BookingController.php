<?php

namespace App\Http\Controllers\FrontWeb\Booking;

use App\Modules\Services\Booking\BookingService;
use App\Modules\Services\Booking\DefaultSettingService;
use App\Modules\Services\Booking\GeneralSettingService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class BookingController extends Controller
{
    protected $defaultBooking;
    protected $specialBooking;
    protected $booking;

    public function __construct(DefaultSettingService  $defaultSetting, GeneralSettingService $generalSettingService, BookingService $booking)
    {
        $this->middleware('customer');
        $this->defaultBooking = $defaultSetting;
        $this->specialBooking = $generalSettingService;
        $this->booking = $booking;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontweb.booking.calendar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAvailableSlots(Request $request)
    {

        $date = $request->get('date');
        $date =Carbon::createFromFormat('d-m-Y', $date)->format('d-m-Y');
        $day =Carbon::createFromFormat('d-m-Y', $date)->format('D');

        $date =Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');

       switch ($day){

           case 'Mon' :
               $day = 0;
               break;
           case 'Tue' :
               $day = 1;
               break;
           case 'Wed' :
               $day = 2;
               break;
           case 'Thu' :
               $day = 3;
               break;
           case 'Fri' :
               $day = 4;
               break;
           case 'Sat' :
               $day = 5;
               break;
           case 'Sun' :
               $day = 6;
               break;
           default:
               $day='NA';
           break;
       }
        $booking = $this->specialBooking->getAvailableSlotsByDay($date);

       if(!empty($booking->isHoliday))
       {
           if($booking->isHoliday=='yes')
           return 'Sorry we are closed on this day.';
       }

       if(empty($booking))
       $booking = $this->defaultBooking->getAvailableSlotsByDay($day);

       if(empty($booking)){
           return 0;
       }
       $spaceAvailable = $booking->space_available;
       $bookingInterval = $booking->booking_interval;
       $totalBooking = $booking->total_booking;
       $openingTime = $booking->start_time;
       $closingTime = $booking->end_time;
       $data=null;
        $nextBookingTime = '';
//        if($day == $booking->day){
//            $bookingOpeningTime = Carbon::createFromTime("HH:mm:ii", $booking->start_time)->addMinutes(180);
//            $openingTime = $bookingOpeningTime;
//        }

        $minutes = $this->get_minutes($openingTime, $closingTime, $bookingInterval);

        foreach($minutes as $minute) {
            if(!empty($minute)) {
                $radioActive = '';
                $spaceAvailableForSlot = 0;
                $bookingDay = $request->get('date') . ' ' . $minute;
                $date = Carbon::parse($bookingDay)->format('Y-m-d h:i:s');
                $time = date('H:i A', strtotime($date));
                $bookedDate = checkNumberOfBookings($date);
                $spaceAvailableForSlot = $spaceAvailable - $bookedDate;
                if ($spaceAvailableForSlot < 1) {
                    $radioActive = 'disabled';
                }

                $data = $data . '
                        <li class="' . $radioActive . '">
                            <label class="custom-control custom-radio">
                                <input type="radio" ' . $radioActive . ' value="' . $minute . '" class="custom-control-input time-radio" name="booking_time">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">' . $time . '</span>
                                <span class="custom-control-space">' . $spaceAvailableForSlot . '  SPACE AVAILABLE</span>
                            </label>
                        </li>';
            }
        }

        return $data;
    }

    function get_minutes ( $start, $end, $bookingInterval ) {
        $minutes[] = '';
        $startTime = $start;
        while ( strtotime($start) <= strtotime($end) ) {
            $minutes[] = date("H:i", strtotime( "$start" ) );
            $txt = $start.' + '.$bookingInterval.' mins';
            $start = date("H:i", strtotime( $txt)) ;

        }
        return $minutes;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveBooking(Request $request)
    {
        if ($this->booking->create($request->all())) {
            $msg = "Your booking has been made. ";
            return view('frontweb.booking.message')->with('msg', 'success');
        }
        return view('frontweb.booking.message')->with('msg', 'error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function bookingStatus(Request $request)
    {
        //
        $msg = 'Ok';
        return view('frontweb.booking.message', compact('msg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function customerBooking()
    {
        $totalBookings = $this->booking->customerTotalBooking(\Auth::user()->id);
        return view('frontweb.mybooking.index', compact('totalBookings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
