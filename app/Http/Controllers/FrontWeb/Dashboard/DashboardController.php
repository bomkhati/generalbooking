<?php

namespace App\Http\Controllers\FrontWeb\Dashboard;

use App\Modules\Services\Booking\BookingService;
use App\Modules\Services\Review\ReviewService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    protected $booking;
    protected $review;

    public function __construct(BookingService  $booking,ReviewService $review)
    {
        $this->middleware('customer');
        $this->booking = $booking;
        $this->review = $review;
    }

    public function index(){

        $approvedBooking = $this->booking->customerApprovedBooking(\Auth::user()->id);
        $pendingBooking = $this->booking->customerPendingBooking(\Auth::user()->id);
        $cancelledBooking = $this->booking->customerCancelledBooking(\Auth::user()->id);
        $reviews = $this->review->customerReview(\Auth::user()->id);

        return view('frontweb.login.dashboard',compact('approvedBooking','pendingBooking','cancelledBooking','reviews'));
    }
}
