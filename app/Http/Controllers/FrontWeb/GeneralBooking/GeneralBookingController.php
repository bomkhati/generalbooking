<?php
namespace App\Http\Controllers\Frontweb\GeneralBooking;

use App\Http\Controllers\Controller;
use App\Modules\Services\Booking\DefaultSettingService;
use App\Modules\Services\Booking\GeneralSettingService;
use App\Modules\Services\SiteSetting\FooterSettingService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GeneralBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $footerSetting;
    protected $defaultBooking;
    protected $specialBooking;

    public function __construct(FooterSettingService $footerSetting,DefaultSettingService  $defaultSetting, GeneralSettingService $generalSettingService)
    {
        $this->footerSetting = $footerSetting;
        $this->defaultBooking = $defaultSetting;
        $this->specialBooking = $generalSettingService;
    }

    public function index()
    {
        $footerSettings = $this->footerSetting->all();
        return view('frontweb.generalBooking.index', compact('footerSettings'));
    }

    public function getAvailableSlots(Request $request)
    {
        $date = $request->get('date');
        $date =Carbon::createFromFormat('d-m-Y', $date)->format('d-m-Y');
        $day =Carbon::createFromFormat('d-m-Y', $date)->format('D');

        $date =Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');

        switch ($day){

            case 'Mon' :
                $day = 0;
                break;
            case 'Tue' :
                $day = 1;
                break;
            case 'Wed' :
                $day = 2;
                break;
            case 'Thu' :
                $day = 3;
                break;
            case 'Fri' :
                $day = 4;
                break;
            case 'Sat' :
                $day = 5;
                break;
            case 'Sun' :
                $day = 6;
                break;
            default:
                $day='NA';
                break;
        }
        $booking = $this->specialBooking->getAvailableSlotsByDay($date);
        if(!empty($booking->isHoliday))
        {
            if($booking->isHoliday=='yes')
                return 'Sorry we are closed on this day.';
        }

        if(empty($booking))
            $booking = $this->defaultBooking->getAvailableSlotsByDay($day);

        if(empty($booking)){
            return 0;
        }
        $spaceAvailable = $booking->space_available;
        $bookingInterval = $booking->booking_interval;
        $totalBooking = $booking->total_booking;
        $openingTime = $booking->start_time;
        $closingTime = $booking->end_time;
        $data=null;
        $nextBookingTime = '';
        $minutes = $this->get_minutes($openingTime, $closingTime, $bookingInterval);


        foreach($minutes as $minute) {
            $radioActive = '';
            $spaceAvailableForSlot = 0;
            $bookingDay = $request->get('date').' '.$minute;
            $date = Carbon::parse($bookingDay)->format('Y-m-d h:i:s');
            $time = date('H:i A', strtotime($date));
            $bookedDate =  checkNumberOfBookings($date);
            $spaceAvailableForSlot = $spaceAvailable - $bookedDate;
            if($spaceAvailableForSlot<1){
                $radioActive = 'disabled';
            }
            $data = $data.'
                        <li class="'.$radioActive.'">
                            <label class="custom-control custom-radio">
                                <input type="radio" '.$radioActive.' value="'.$minute.'" class="custom-control-input time-radio" name="booking_time">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">'.$time.'</span>
                                <span class="custom-control-space">'.$spaceAvailableForSlot.'  SPACE AVAILABLE</span>
                            </label>
                        </li>';
        }
        return $data;
    }

    function get_minutes ( $start, $end, $bookingInterval ) {

        while ( strtotime($start) <= strtotime($end) ) {
            $minutes[] = date("H:i", strtotime( "$start" ) );
            $txt = $start.' + '.$bookingInterval.' mins';
            $start = date("H:i", strtotime( $txt)) ;
        }
        return $minutes;
    }


    public function saveBooking(Request $request)
    {
        if ($this->booking->create($request->all())) {
            $msg = "Your booking has been made. ";
            return view('frontweb.generalBooking.message')->with('msg', 'success');
        }
        return view('frontweb.generalBooking.message')->with('msg', 'error');
    }

}
