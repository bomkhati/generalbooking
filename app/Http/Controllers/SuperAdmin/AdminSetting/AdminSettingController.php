<?php

namespace App\Http\Controllers\SuperAdmin\AdminSetting;

use App\Http\Requests\AdminChangePasswordRequest;
use App\Http\Requests\AdminLoginAddRequest;
use App\Http\Requests\AdminLoginEditRequest;
use App\Modules\Models\User\User;
use App\Modules\Services\Login\AdminSettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminSettingController extends Controller
{

    protected $login;

    public function __construct(AdminSettingService $login)
    {
        $this->middleware('superadmin');
        $this->login = $login;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('*')->where('user_type', '=', 'superadmin')->paginate('10');
        return view('superadmin.admin-setting.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->login->find($id);
        return view('superadmin.admin-setting.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update( $request, $id)
    {

        if ($this->login->update($id, $request->all())) {
            return redirect()->route('admin-setting.index')->with('success', 'Admin Info updated successfully.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPasswordChangeForm($id)
    {
        $user = $this->login->find($id);
        return view('superadmin.admin-setting.change-password', compact('user'));
    }


    public function passwordChange(AdminChangePasswordRequest $request, $id)
    {

        $user = $this->login->find($id);
        $oldPassword = $request->get('old_password');
        $newPassword['password'] = bcrypt($request->get('password'));

        if (!Hash::check($oldPassword, $user->password)) {
            return back()->with('error', 'Please Enter Valid Old Password');
        } else {
            if ($this->login->update($user->id, $newPassword))
                return redirect()->back()->with('success', 'Password Updated Successfully');
            else
                return redirect()->back()->with('error', 'Password Cannot Be Updated');
        }

    }

}
