<?php namespace App\Http\Controllers\SuperAdmin\Option;

use App\Http\Controllers\Controller;
use App\Modules\Services\Option\GeneralOptionService;
use Illuminate\Http\Request;

class GeneralOptionController extends Controller
{
    protected $option;

    public function __construct(GeneralOptionService $option
    )
    {
        $this->middleware('superadmin');
        $this->option = $option;
    }

    /**
     * write brief description
     * @return mixed
     */
    public function index()
    {
        $options = $this->option->paginate();
        return view('superadmin.option.index', compact('options'));
    }

    /**
     *
     */
    public function create()
    {
        return view('superadmin.option.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param PageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($this->option->create($request->all())) {
            return redirect()->route('option.index')->with('success', ' General Option added successfully.');
        }

        return redirect()->route('option.create')->with('error', 'General Option could not be created.');
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $option = $this->option->find($id);
        return view('superadmin.option.edit', compact('option'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param Request $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if($this->option->update($id, $request->all())) {
            return redirect()->route('option.index')->with('success', 'General Option updated successfully.');
        }

        return redirect()->route('option.edit',[$id])->with('error', 'General Option could not be updated.');
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->option->delete($id)) {
            return redirect()->route('option.index')->with('success', 'General Option deleted successfully.');
        }

        return redirect()->route('option.index')->with('error', 'Due to some technical issues, unable to process the request. Contact Tech Support');
    }
}