<?php namespace App\Http\Controllers\SuperAdmin\Gallery;

use App\Http\Controllers\Controller;
use App\Http\Requests\Gallery\GalleryRequest;
use App\Modules\Services\Gallery\GalleryService;
use Illuminate\Http\Request;


class GalleryController extends Controller
{
    protected $gallery;
    public function __construct(GalleryService $gallery)
    {
        $this->middleware('superadmin');
        $this->gallery = $gallery;
    }

    /**
     * write brief description
     * @return mixed
     */
    public function index()
    {
        $galleries = $this->gallery->paginate();
        return view('superadmin.gallery.index', compact('galleries'));
    }

    /**
     *
     */
    public function create()
    {
        return view('superadmin.gallery.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param GalleryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(GalleryRequest $request)
    {
        if ($this->gallery->create($request->all())) {
            return redirect()->route('gallery.index')->with('success', 'Gallery created successfully.');
        }

        return redirect()->route('gallery.create')->with('error', 'Gallery could not be created.');
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = $this->gallery->find($id);
        return view('superadmin.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param GalleryRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(GalleryRequest $request, $id)
    {
        if($this->gallery->update($id, $request->all())) {
            return redirect()->route('gallery.index')->with('success', 'Gallery updated successfully.');
        }

        return redirect()->route('gallery.edit',[$id])->with('error', 'Gallery could not be updated.');
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->gallery->delete($id)) {
            return redirect()->route('gallery.index')->with('success', 'Gallery deleted successfully.');
        }

        return redirect()->route('gallery.index')->with('error', 'Gallery could not be deleted.');
    }
}