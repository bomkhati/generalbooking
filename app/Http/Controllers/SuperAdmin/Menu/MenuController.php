<?php namespace App\Http\Controllers\SuperAdmin\Menu;

use App\Http\Controllers\HomeController;
use App\Http\Requests\User\UserRequest;
use App\Modules\Services\User\UserService;

class MenuController extends HomeController
{
    protected $user;

    public function __construct(
        UserService $user
    )
    {
        $this->middleware('superadmin');
        $this->user = $user;
    }

    /**
     * write brief description
     * @return mixed
     */
    public function index()
    {
        $users = $this->user->paginate();
        return view('superadmin.menu.index');
    }

    /**
     *
     */
    public function create()
    {
        return view('superadmin.user.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        if ($this->user->create($request->all())) {
            return redirect()->route('user.index')->with('success', 'Customer created successfully.');
        }
        return redirect()->route('user.create')->with('error', 'Customer could not be created.');
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);
        return view('superadmin.user.edit', compact('user'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param UserRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $id)
    {
        if($this->user->update($id, $request->all())) {
            return redirect()->route('user.index')->with('success', 'Customer updated successfully.');
        }

        return redirect()->route('user.edit',[$id])->with('error', 'Customer could not be updated.');
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->user->delete($id)) {
            return redirect()->route('user.index')->with('success', 'Customer deleted successfully.');
        }

        return redirect()->route('user.index')->with('error', 'Customer could not be deleted.');
    }
}