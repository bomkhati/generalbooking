<?php namespace App\Http\Controllers\SuperAdmin\Facility;

use App\Http\Controllers\Controller;
use App\Http\Requests\Facility\FacilityRequest;
use App\Modules\Services\Facility\FacilityService;

class FacilityController extends Controller
{
    protected $facility;
    public function __construct(FacilityService $facility)
    {
        $this->middleware('superadmin');
        $this->facility = $facility;
    }

    /**
     * write brief description
     * @return mixed
     */
    public function index()
    {
        $facilities = $this->facility->paginate();
        return view('superadmin.facility.index', compact('facilities'));
    }

    /**
     *
     */
    public function create()
    {
        return view('superadmin.facility.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param FacilityRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FacilityRequest $request)
    {
        if ($this->facility->create($request->all())) {
            return redirect()->route('facility.index')->with('success', 'Facility created successfully.');
        }

        return redirect()->route('facility.create')->with('error', 'Facility could not be created.');
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = $this->facility->find($id);
        return view('superadmin.facility.edit', compact('facility'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param FacilityRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FacilityRequest $request, $id)
    {
        if($this->facility->update($id, $request->all())) {
            return redirect()->route('facility.index')->with('success', 'Facility updated successfully.');
        }

        return redirect()->route('facility.edit',[$id])->with('error', 'Facility could not be updated.');
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->facility->delete($id)) {
            return redirect()->route('facility.index')->with('success', 'Facility deleted successfully.');
        }

        return redirect()->route('facility.index')->with('error', 'Facility could not be deleted.');
    }
}