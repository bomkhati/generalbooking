<?php

namespace App\Http\Controllers\SuperAdmin\Dashboard;

use App\Modules\Services\Booking\BookingService;
use App\Http\Controllers\Controller;
use App\Modules\Services\Review\ReviewService;
use App\Modules\Services\User\UserService;

class DashboardController extends Controller
{
    protected $booking;
    protected $review;
    protected $customer;

    public function __construct(BookingService $booking,ReviewService $review,UserService $customer)
    {
        $this->middleware('superadmin');
        $this->booking = $booking;
        $this->review = $review;
        $this->customer = $customer;
    }


    public function index(){

        $booking = $this->booking->all();
        $review = $this->review->all();
        $customer = $this->customer->all();

        return view('superadmin.dashboard.index',compact('booking','review','customer'));
    }

}
