<?php

namespace App\Http\Controllers\SuperAdmin\SiteSetting;

use App\Modules\Services\SiteSetting\SiteMaintenanceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SiteMaintenanceController extends Controller
{
    protected $siteMaintenance;

    function __construct(SiteMaintenanceService $siteMaintenance)
    {
        $this->siteMaintenance =$siteMaintenance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maintenances = $this->siteMaintenance->paginate();
        return view('superadmin.site-setting.maintenance.index',compact('maintenances'));
    }

    public function edit($id)
    {
       $maintenance = $this->siteMaintenance->find($id);
       return view('superadmin.site-setting.maintenance.edit',compact('maintenance'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $password = $request->get('password');
        //dd($user->password);
        if(!Hash::check($password,$user->password))
        {
            return back()->with('error','Please Enter Valid  Password');
        }
        else
        {
            if($this->siteMaintenance->update($id, $request->all())) {
                return redirect()->route('site-maintenance.index')->with('success', 'Maintenance Changed successfully.');
            }

            return redirect()->route('site-maintenance.index',[$id])->with('error', 'Maintenance could not be changed.');

        }

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
