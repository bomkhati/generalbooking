<?php

namespace App\Http\Controllers\SuperAdmin\SiteSetting;

use App\Http\Controllers\Controller;
use App\Http\Requests\SiteNoticeRequest;
use App\Modules\Services\SiteSetting\SiteNoticeService;
use Illuminate\Http\Request;

class SiteNoticeController extends Controller
{
    protected $siteNotice;
    function __construct(SiteNoticeService $siteNotice)
    {
        $this->siteNotice = $siteNotice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = $this->siteNotice->paginate();
        return view('superadmin.site-setting.notice.index', compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.site-setting.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteNoticeRequest $request)
    {
        if ($this->siteNotice->create($request->all())) {
            return redirect()->route('site-notice.index')->with('success', 'Site-Notice created successfully.');
        }

        return redirect()->route('site-notice.create')->with('error', 'Site-Notice could not be created.');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice =$this->siteNotice->find($id);
        return view('superadmin.site-setting.notice.edit',compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteNoticeRequest $request, $id)
    {

        if($this->siteNotice->update($id, $request->all())) {
            return redirect()->route('site-notice.index')->with('success', 'Site-Notice updated successfully.');
        }

        return redirect()->route('site-notice.edit',[$id])->with('error', 'Site-Notice could not be updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->siteNotice->delete($id)) {

            return redirect()->route('site-notice.index')->with('success', 'Site-Notice deleted successfully.');
        }

        return redirect()->route('site-notice.index')->with('error', 'Site-Notice could not be deleted.');
    }
}
