<?php

namespace App\Http\Controllers\SuperAdmin\SiteSetting;

use App\Modules\Services\SiteSetting\FooterSettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FooterSettingController extends Controller
{
    protected $footerSetting;

    function __construct(FooterSettingService  $footerSetting)
    {
        $this->footerSetting =$footerSetting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $footerSettings = $this->footerSetting->paginate();
        return view('superadmin.site-setting.footer.index',compact('footerSettings'));
    }

    public function edit($id)
    {
        $footerSetting = $this->footerSetting->find($id);
        return view('superadmin.site-setting.footer.edit',compact('footerSetting'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->footerSetting->update($id, $request->all())) {
            return redirect()->route('footer-setting.index')->with('success', 'Footer Settings Changed successfully.');
        }
        return redirect()->route('footer-setting.edit',[$id])->with('error', 'Footer Settings could not be changed.');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $footerSetting = $this->footerSetting->all();
        return view('superadmin.site-setting.footer.create' , compact('footerSetting'));
    }

    public function store(Request $request)
    {
        if ($this->footerSetting->create($request->all())) {
            return redirect()->route('footer-setting.index')->with('success', 'Footer Settings created successfully.');
        }
        return redirect()->route('footer-setting.create')->with('error', 'Footer Settings could not be created.');
    }


    public function destroy($id)
    {
        if ($this->footerSetting->delete($id)) {
            return redirect()->route('footer-setting.index')->with('success', 'Footer Settings deleted successfully.');
        }
        return redirect()->route('footer-setting.index')->with('error', 'Footer Settings could not be deleted.');
    }
}
