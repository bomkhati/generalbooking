<?php

namespace App\Http\Controllers\SuperAdmin\SiteSetting;

use App\Modules\Services\SiteSetting\TrackingCodeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackingCodeController extends Controller
{
    protected $trackingCode;

    function __construct(TrackingCodeService  $trackingCode)
    {
        $this->trackingCode =$trackingCode;
    }

    public function index()
    {
        $trackingCodes = $this->trackingCode->paginate();
        return view('superadmin.site-setting.trackingCode.index',compact('trackingCodes'));
    }

    public function edit($id)
    {
        $trackingCode = $this->trackingCode->find($id);
        return view('superadmin.site-setting.trackingCode.edit',compact('trackingCode'));
    }

    public function update(Request $request, $id)
    {
        if($this->trackingCode->update($id, $request->all())) {
            return redirect()->route('tracking-code.index')->with('success', 'Tracking codes Changed successfully.');
        }
        return redirect()->route('tracking-code.edit',[$id])->with('error', 'Tracking codes could not be changed.');
    }

    public function create()
    {
        $trackingCode = $this->trackingCode->all();
        return view('superadmin.site-setting.trackingCode.create' , compact('trackingCode'));
    }

    public function store(Request $request)
    {
        if ($this->trackingCode->create($request->all())) {
            return redirect()->route('tracking-code.index')->with('success', 'Tracking codes created successfully.');
        }
        return redirect()->route('tracking-code.create')->with('error', 'Tracking codes could not be created.');
    }


    public function destroy($id)
    {
        if ($this->trackingCode->delete($id)) {
            return redirect()->route('tracking-code.index')->with('success', 'Tracking codes deleted successfully.');
        }
        return redirect()->route('tracking-code.index')->with('error', 'Tracking codes could not be deleted.');
    }
}
