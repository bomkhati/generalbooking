<?php

namespace App\Http\Controllers\SuperAdmin\Review;

use App\Http\Requests\Review\ReviewRequest;
use App\Modules\Services\Review\ReviewService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    protected $review;

    function __construct(ReviewService  $review)
    {
        $this->review =$review;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = $this->review->paginate();
        return view('superadmin.review.index',compact('reviews'));
    }


    public function edit($id)
    {
        $review = $this->review->find($id);
        return view('superadmin.review.edit',compact('review'));
    }

    public function create()
    {
        return view('superadmin.review.create');
    }


    public function store(ReviewRequest $request)
    {
        if ($this->review->create($request->all())) {
            return redirect()->route('review.index')->with('success', 'Review created successfully.');
        }

        return redirect()->route('review.create')->with('error', 'Review could not be created.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->review->update($id, $request->all())) {
            return redirect()->route('review.index')->with('success', 'Review changed successfully.');
        }
        return redirect()->route('review.edit',[$id])->with('error', 'Review could not be changed.');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        if ($this->review->delete($id)) {
            return redirect()->route('review.index')->with('success', 'Review deleted successfully.');
        }
        return redirect()->route('review.index')->with('error', 'Review could not be deleted.');
    }


    public function reviewUpdate(Request $request)
    {
        $reviewId = $request->reviewId;
        $status = $request->status;
        $review = $this->review->find($reviewId);
        $data ['status'] = $status;
        if($review->update($data)) {
            return 1;
        }else{
            return 0;
        }
    }
}
