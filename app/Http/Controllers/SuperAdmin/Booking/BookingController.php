<?php

namespace App\Http\Controllers\SuperAdmin\Booking;

use App\Mail\BookingApproved;
use App\Modules\Services\Booking\BookingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{
    protected $booking;

    function __construct(BookingService  $booking)
    {
        $this->booking =$booking;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = $this->booking->paginate();
        return view('superadmin.booking.customer-booking.index',compact('bookings'));
    }

    public function edit($id)
    {
        $booking = $this->booking->find($id);
        return view('superadmin.booking.customer-booking.edit',compact('booking'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->booking->update($id, $request->all())) {
            return redirect()->route('customer-booking.index')->with('success', 'Booking status changed successfully.');
        }
        return redirect()->route('customer-booking.edit',[$id])->with('error', 'Booking status could not be changed.');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        if ($this->booking->delete($id)) {
            return redirect()->route('customer-booking.index')->with('success', 'Booking status deleted successfully.');
        }
        return redirect()->route('customer-booking.index')->with('error', 'Booking status could not be deleted.');
    }

    public function statusUpdate(Request $request)
    {
        $bookingId = $request->bookingId;
        $status = $request->status;
        $booking = $this->booking->find($bookingId);
        $data ['status'] = $status;
        if($booking->update($data)) {
            $email = $booking->customers->email;
            if (!empty($booking)) {
                try{
                    Mail::to($email)->send(new BookingApproved($booking));
                }catch (Exception $e){

                }
            }
            return $booking;
            return 1;
        }else{
            return 0;
            }
        }
}
