<?php

namespace App\Http\Controllers\SuperAdmin\Booking;

use App\Modules\Services\Booking\DefaultSettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefaultSettingController extends Controller
{
    protected $defaultSetting;

    function __construct(DefaultSettingService  $defaultSetting)
    {
        $this->defaultSetting =$defaultSetting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $defaultSettings = $this->defaultSetting->paginate();
        return view('superadmin.booking.default-setting.index',compact('defaultSettings'));
    }

    public function edit($id)
    {
        $defaultSetting = $this->defaultSetting->find($id);
        return view('superadmin.booking.default-setting.edit',compact('defaultSetting'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->defaultSetting->update($id, $request->all())) {
            return redirect()->route('default-setting.index')->with('success', 'Booking Default Settings Changed successfully.');
        }
        return redirect()->route('default-setting.edit',[$id])->with('error', 'Booking Default Settings could not be changed.');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $defaultSetting = $this->defaultSetting->all();
        return view('superadmin.booking.default-setting.create' , compact('defaultSetting'));
    }

    public function store(Request $request)
    {
        if ($this->defaultSetting->create($request->all())) {
            return redirect()->route('default-setting.index')->with('success', 'Booking Default Settings created successfully.');
        }
        return redirect()->route('default-setting.create')->with('error', 'Booking Default Settings could not be created.');
    }


    public function destroy($id)
    {
        if ($this->defaultSetting->delete($id)) {
            return redirect()->route('default-setting.index')->with('success', 'Booking Default Settings deleted successfully.');
        }
        return redirect()->route('default-setting.index')->with('error', 'Booking Default Settings could not be deleted.');
    }
}
