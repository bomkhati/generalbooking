<?php

namespace App\Http\Controllers\SuperAdmin\Booking;

use App\Http\Controllers\Controller;
use App\Http\Requests\Booking\SubCategoryRequest;
use App\Modules\Services\Booking\CategoryService;
use App\Modules\Services\Booking\SubCategoryService;


class SubCategoryController extends Controller
{

    protected $category;
    protected $subcategory;

    public function __construct(CategoryService $category, SubCategoryService $subcategory)
    {
        $this->middleware('superadmin');
        $this->category = $category;
        $this->subcategory = $subcategory;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = $this->subcategory->paginate();
        return view('superadmin.booking.subcategory.index', compact('subcategories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->all();
        return view('superadmin.booking.subcategory.create', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request)
    {
        if ($this->subcategory->create($request->all())) {
            return redirect()->route('subcategory.index')->with('success', 'Sub Category created successfully.');
        }

        return redirect()->route('subcategory.create')->with('error', 'Sub Category could not be created.');

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories =$this->category->all();
        $subcategory = $this->subcategory->find($id);
        $savedCategory = $subcategory->category;

        return view('superadmin.booking.subcategory.edit', compact('subcategory','categories','savedCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, $id)
    {

        if($this->subcategory->update($id, $request->all())) {
            return redirect()->route('subcategory.index')->with('success', 'Sub Category updated successfully.');
        }

        return redirect()->route('subcategory.edit',[$id])->with('error', 'Sub Category could not be updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->subcategory->delete($id)) {
            return redirect()->route('subcategory.index')->with('success', 'Sub Category deleted successfully.');
        }

        return redirect()->route('subcategory.index')->with('error', 'Sub Category could not be deleted.');
    }

}
