<?php

namespace App\Http\Controllers\SuperAdmin\Booking;

use App\Modules\Services\Booking\GeneralSettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralSettingController extends Controller
{
    protected $generalSetting;

    function __construct(GeneralSettingService  $generalSetting)
    {
        $this->generalSetting =$generalSetting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $generalSettings = $this->generalSetting->paginate();
        return view('superadmin.booking.general-setting.index',compact('generalSettings'));
    }

    public function edit($id)
    {
        $generalSetting = $this->generalSetting->find($id);
        return view('superadmin.booking.general-setting.edit',compact('generalSetting'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->generalSetting->update($id, $request->all())) {
            return redirect()->route('general-setting.index')->with('success', 'General Settings Changed successfully.');
        }
        return redirect()->route('general-setting.edit',[$id])->with('error', 'General Settings could not be changed.');
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $generalSetting = $this->generalSetting->all();
        return view('superadmin.booking.general-setting.create' , compact('generalSetting'));
    }

    public function store(Request $request)
    {
        if ($this->generalSetting->create($request->all())) {
            return redirect()->route('general-setting.index')->with('success', 'General Settings created successfully.');
        }
        return redirect()->route('general-setting.create')->with('error', 'General Settings could not be created.');
    }


    public function destroy($id)
    {
        if ($this->generalSetting->delete($id)) {
            return redirect()->route('general-setting.index')->with('success', 'General Settings deleted successfully.');
        }
        return redirect()->route('general-setting.index')->with('error', 'General Settings could not be deleted.');
    }
}
