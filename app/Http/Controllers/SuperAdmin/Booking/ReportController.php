<?php

namespace App\Http\Controllers\SuperAdmin\Booking;

use App\Http\Controllers\SuperAdmin\AdminBaseController;
use App\Modules\Models\Booking\Booking\Booking;
use App\Modules\Services\Booking\BookingService;
use App\Modules\Services\Booking\DefaultSettingService;
use App\Modules\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ReportController extends Controller
{
    protected $search_params = [];
    protected $user;
    protected $booking;
    protected $bookingSetting;

    function __construct(UserService $user, BookingService $booking, DefaultSettingService $bookingSetting)
    {
        $this->user = $user;
        $this->booking = $booking;
        $this->bookingSetting = $bookingSetting;
    }

    public function index(Request $request)
    {
        if (empty($request->get('end_date')))
            $endDate = date('Y-m-d');
        else
            $endDate = $request->get('end_date');

        $totalBooking = 0;
        $endDate=Carbon::parse($endDate)->addDay(1)->toDateString();
        $bookings = null;

        if($request->has('begin_date')):
            $bookings = Booking::select('*')->where(function ($query) use ($request, $endDate) {

                if ($request->has('begin_date')) {

                    $query->whereBetween('time_slots', [$request->get('begin_date'), $endDate]);
                    $this->search_params['begin_date'] = $request->get('begin_date');
                    $this->search_params['end_date'] = $endDate;
                }
                if (!empty($request->get('month'))) {

                    $query->whereMonth('time_slots', '=', $request->get('month'));
                    $this->search_params['month'] = $request->get('month');
                }


                if (!empty($request->get('status'))) {
                    $query->where('status', '=', $request->get('status'));
                    $this->search_params['status'] = $request->get('status');
                }



            })->paginate(50);

        endif;


            $totalBooking = Booking::select('*'
            )->where(function ($query) use ($request, $endDate) {

                if ($request->has('begin_date')) {

                    $query->whereBetween('time_slots', [$request->get('begin_date'), $endDate]);
                    $this->search_params['begin_date'] = $request->get('begin_date');
                    $this->search_params['end_date'] = $endDate;
                }
                if (!empty($request->get('month'))) {
                    $query->whereMonth('time_slots', '=', $request->get('month'));
                    $this->search_params['month'] = $request->get('month');
                }


                if (!empty($request->get('status'))) {
                    $query->where('status', '=', $request->get('status'));
                    $this->search_params['status'] = $request->get('status');
                }



            })->count();


        $search_params = $this->search_params;



        return view('superadmin.booking.reports.index', compact('bookings', 'search_params','totalBooking'));
    }


}
