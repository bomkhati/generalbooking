<?php namespace App\Http\Controllers\SuperAdmin\Page;

use App\Http\Controllers\Controller;
use App\Http\Requests\Page\PageRequest;
use App\Modules\Services\Page\PageService;


class PageController extends Controller
{
    protected $page;
    public function __construct(PageService $page)
    {
        $this->middleware('superadmin');
        $this->page = $page;
    }

    /**
     * write brief description
     * @return mixed
     */
    public function index()
    {
        $pages = $this->page->paginate();
        return view('superadmin.page.index', compact('pages'));
    }

    /**
     *
     */
    public function create()
    {
        return view('superadmin.page.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param PageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PageRequest $request)
    {
        if ($this->page->create($request->all())) {
            return redirect()->route('page.index')->with('success', 'Page created successfully.');
        }

        return redirect()->route('page.create')->with('error', 'Page could not be created.');
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->page->find($id);
        return view('superadmin.page.edit', compact('page'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param PageRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PageRequest $request, $id)
    {
        if($this->page->update($id, $request->all())) {
            return redirect()->route('page.index')->with('success', 'Page updated successfully.');
        }

        return redirect()->route('page.edit',[$id])->with('error', 'Page could not be updated.');
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->page->delete($id)) {
            return redirect()->route('page.index')->with('success', 'Page deleted successfully.');
        }

        return redirect()->route('page.index')->with('error', 'Page could not be deleted.');
    }
}