<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\SuperAdminLoginController;
use Closure;
use Illuminate\Support\Facades\Auth;
class SuperAdminMiddleware
{
    protected  $login;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    function __construct(SuperAdminLoginController $login)
    {
        $this->login=$login;
        Auth::shouldUse('superadmin');

    }

    public function handle($request, Closure $next, $guard = null)
    {
        //Auth::logout();
        Auth::guard('superadmin');
        $user = Auth::user();
        if( Auth::guard($guard)->check()) {
            if (empty($user))
            {

                if (!strstr($request->url(), 'login'))
                {
                    return redirect('/administrator/login');
                }
            }

            else
            {
                route('superadmin.dashboard');
            }
        }

        else
        {
            if (!strstr($request->url(), 'login'))


                return redirect('/administrator/login');

        }
        return $next($request);

    }

}
