<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\LoginController;
use Closure;
use Illuminate\Support\Facades\Auth;
class CustomerMiddleware
{
    protected  $login;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    function __construct(LoginController $login)
    {
        $this->login=$login;
        Auth::shouldUse('customer');

    }

    public function handle($request, Closure $next, $guard = null)
    {
        //Auth::logout();
        Auth::guard('customer');
        $user = Auth::user();
        if( Auth::guard($guard)->check()) {
            if (empty($user))
            {

                if (!strstr($request->url(), 'login'))
                {
                    return redirect('/customer/login');
                }
            }

            else
            {
                route('customer.dashboard');
            }
        }

        else
        {
            if (!strstr($request->url(), 'login'))


                return redirect('/customer/login');

        }
        return $next($request);

    }

}
