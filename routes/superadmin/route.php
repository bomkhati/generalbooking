<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'superadmin', 'prefix' => 'administrator'], function ($router) {
    $router->get('/',
        ['as' => 'superadmin.login', 'uses' => 'Auth\SuperAdminLoginController@showLoginForm']);
    $router->get('login',
        ['as' => 'superadmin.login', 'uses' => 'Auth\SuperAdminLoginController@showLoginForm']);

    $router->get('dashboard',
        ['as' => 'superadmin.dashboard', 'uses' => 'SuperAdmin\Dashboard\DashboardController@index']);
    $router->post('login',
        ['as' => 'superadmin.login', 'uses' => 'Auth\SuperAdminLoginController@login']);
    $router->get('logout',
        ['as' => 'superadmin.logout', 'uses' => 'Auth\SuperAdminLoginController@logout']);
    $router->resource('admin-setting', 'SuperAdmin\AdminSetting\AdminSettingController');
    Route::get('admin-setting/{id}/change-password',
        ['as' => 'admin-setting.change-password', 'uses' => 'SuperAdmin\AdminSetting\AdminSettingController@showPasswordChangeForm']);
    Route::post('admin-setting/{id}/change-password',
        ['as' => 'admin-setting.change-password', 'uses' => 'SuperAdmin\AdminSetting\AdminSettingController@passwordChange']);

    $router->get('customer-booking/statusupdate',
        ['as' => 'superadmin.ajax.booking.status', 'uses' => 'SuperAdmin\Booking\BookingController@statusUpdate']);
    $router->get('customer-reiew/reviewupdate',
        ['as' => 'superadmin.ajax.review.status', 'uses' => 'SuperAdmin\Review\ReviewController@reviewUpdate']);

    $router->resource('site-notice', 'SuperAdmin\SiteSetting\SiteNoticeController');
    $router->resource('site-maintenance', 'SuperAdmin\SiteSetting\SiteMaintenanceController');
    $router->resource('footer-setting', 'SuperAdmin\SiteSetting\FooterSettingController');
    $router->resource('option', 'SuperAdmin\Option\GeneralOptionController');
    $router->resource('page', 'SuperAdmin\Page\PageController');
    $router->resource('facility', 'SuperAdmin\Facility\FacilityController');
    $router->resource('faq', 'SuperAdmin\Faq\FaqController');
    $router->resource('faq-category', 'SuperAdmin\Faq\FaqCategoryController');
    $router->resource('banner', 'SuperAdmin\Banner\BannerController');
    $router->resource('default-setting', 'SuperAdmin\Booking\DefaultSettingController');
    $router->resource('general-setting', 'SuperAdmin\Booking\GeneralSettingController');
    $router->resource('gallery', 'SuperAdmin\Gallery\GalleryController');
    $router->resource('customer-booking', 'SuperAdmin\Booking\BookingController');
    $router->resource('user', 'SuperAdmin\User\UserController');
    $router->resource('review', 'SuperAdmin\Review\ReviewController');
    $router->resource('tracking-code', 'SuperAdmin\SiteSetting\TrackingCodeController');
    $router->resource('category', 'SuperAdmin\Booking\CategoryController');
    $router->resource('subcategory', 'SuperAdmin\Booking\SubCategoryController');
    $router->get('reports', 'SuperAdmin\Booking\ReportController@index')->name('reports');


});
