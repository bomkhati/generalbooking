<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once('superadmin/route.php');
//include_once('frontWeb/route.php');


Route::group(['middleware' => 'customer', 'prefix' => 'customer'], function ($router) {
    $router->get('login', ['as' => 'customer.login', 'uses' => 'Auth\CustomerLoginController@showLoginForm']);

    $router->get('dashboard',
        ['as' => 'customer.dashboard', 'uses' => 'FrontWeb\Dashboard\DashboardController@index']);
    $router->post('login',
        ['as' => 'customer.login', 'uses' => 'Auth\CustomerLoginController@login']);
    $router->get('logout',
        ['as' => 'customer.logout', 'uses' => 'Auth\CustomerLoginController@logout']);
    $router->get('/booking', 'FrontWeb\Booking\BookingController@index')->name('booking');
    $router->get('/my-booking', 'FrontWeb\Booking\BookingController@customerBooking')->name('my-booking');
    $router->resource('my-review', 'FrontWeb\Review\ReviewController');

    $router->get('get-available-slots-by-date')->name('get-available-slots-by-name');
    $router->get('/get-available-slots-by-date', 'FrontWeb\Booking\BookingController@getAvailableSlots')->name('available.slots');

    $router->post('booking', 'FrontWeb\Booking\BookingController@saveBooking')->name('booking.request');

    $router->get('booking/status', 'FrontWeb\Booking\BookingController@bookingStatus')->name('booking.status');


});

Route::get('/general-booking',['as' => 'general-booking', 'uses' => 'Frontweb\GeneralBooking\GeneralBookingController@index']);
Route::get('get-available-slots-guest',['as' => 'general-booking-guest', 'uses' => 'Frontweb\GeneralBooking\GeneralBookingController@getAvailableSlots']);
//Route::post('get-available-slots-guest',['as' => 'save-general-booking-guest', 'uses' => 'Frontweb\GeneralBooking\GeneralBookingController@saveBooking']);
Route::get('/contact',['as' => 'contact', 'uses' => 'Frontweb\Contact\ContactController@index']);
Route::get('/','Auth\CustomerLoginController@showLoginForm');

Route::post('save/guest/booking', 'FrontWeb\Booking\GuestBookingController@saveBooking')->name('save-guest-booking');


//after
Auth::routes(['verify' => true]);

